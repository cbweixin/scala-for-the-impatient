package chapt4

import java.util.Scanner

import scala.collection.mutable
import scala.collection.immutable
import scala.collection.mutable.ListBuffer

object Exercises {

  /**
    * Ex 1:
    *
    * Set up a map of prices for a number of gizmos that you covet. Then produce a second map with the same keys and
    * the prices at a 10 percent discount.
    *
    * @return
    */
  private def reducedPrices(): Map[String, Int] = {
    val gizmos = Map("iPhone" -> 600,
      "iPad" -> 500,
      "MacBook Pro" -> 2000,
      "ScalaDays 2016 Berlin" -> 750)
    gizmos.mapValues(price => price - price / 10)
  }

  /**
    * Ex 2:
    *
    * Set up a map of prices for a number of gizmos that you covet. Then produce a second map with the same keys and
    * the prices at a 10 percent discount.
    *
    * @return
    */
  private def reducedPrices2(): Map[String, Int] = {
    val gizmos = Map("iPhone" -> 600,
      "iPad" -> 500,
      "MacBook Pro" -> 2000,
      "ScalaDays 2016 Berlin" -> 750)
    for ((k, v) <- gizmos) yield (k, v - v / 10)
  }

  /**
    * ex 2:
    *
    * Write a program that reads words from a file. Use a mutable map to count how often each word appears. To read
    * the words, simply use a java.util.Scanner:
    * val in = new java.util.Scanner(new java.io.File("myfile.txt")) while (in.hasNext()) process in.next()
    * Or look at Chapter 9 for a Scalaesque way.
    *
    * @return
    */
  private def countWordsInFile(): mutable.Map[String, Int] = {
    val words = new mutable.HashMap[String, Int]
    val in = new Scanner(getClass.getResourceAsStream("/myfile.txt"))
    try {
      while (in.hasNext()) {
        words(in.next()) = words.getOrElse(in.next(), 0) + 1
      }
    } finally {
      in.close()
    }
    words
  }

  private def countWordsInFile2(): mutable.Map[String, Int] = {
    val words = new mutable.HashMap[String, Int]
    processWords(w => words(w) = words.getOrElse(w, 0) + 1)
    words
  }

  /**
    * Ex 3:
    *
    * Repeat the preceding exercise with an immutable map.
    *
    * @return
    */
  private def countWordsWithImutableMap(): immutable.Map[String, Int] = {
    var words = new immutable.HashMap[String, Int]
    val in = new Scanner(getClass.getResourceAsStream("/myfile.txt"))
    try {
      while (in.hasNext()) {
        words += (in.next() -> (words.getOrElse(in.next(), 0) + 1))
      }
    } finally {
      in.close()
    }
    words
  }

  private def countWordsWithImutableMap2(): immutable.Map[String, Int] = {
    var words = new immutable.HashMap[String, Int]
    processWords(w => words += w -> (words.getOrElse(w, 0) + 1))
    words
  }

  /**
    * ex  4
    *
    * Repeat the preceding exercise with a sorted map, so that the words are printed in sorted order.
    *
    * @param
    */
  private def countWordsWithSortedMap(): mutable.SortedMap[String, Int] = {
    var words = mutable.SortedMap[String, Int]()
    val in = new Scanner(getClass.getResourceAsStream("/myfile.txt"))
    try {
      while (in.hasNext()) {
        words(in.next()) = words.getOrElse(in.next(), 0) + 1
      }
    } finally {
      in.close()
    }
    words
  }

  /**
    * ex  4
    *
    * Repeat the preceding exercise with a sorted map, so that the words are printed in sorted order.
    *
    * @param
    */
  private def countWordsWithImutabeSortedMap()
  : immutable.SortedMap[String, Int] = {
    var words = immutable.SortedMap[String, Int]()
    val in = new Scanner(getClass.getResourceAsStream("/myfile.txt"))
    try {
      while (in.hasNext()) {
        words += (in.next() -> (words.getOrElse(in.next(), 0) + 1))
      }
    } finally {
      in.close()
    }
    words
  }

  /**
    * ex  5
    *
    * Repeat the preceding exercise with a tree map, so that the words are printed in sorted order.
    *
    * @param
    */
  private def countWordsWithImutabeTreeMap(): immutable.TreeMap[String, Int] = {
    var words = immutable.TreeMap[String, Int]()
    val in = new Scanner(getClass.getResourceAsStream("/myfile.txt"))
    try {
      while (in.hasNext()) {
        words += (in.next() -> (words.getOrElse(in.next(), 0) + 1))
      }
    } finally {
      in.close()
    }
    words
  }

  /**
    * ex  5
    *
    * Repeat the preceding exercise with a tree map, so that the words are printed in sorted order.
    *
    * @param
    */
  private def countWordsWithTreeMap(): mutable.TreeMap[String, Int] = {
    var words = mutable.TreeMap[String, Int]()
    val in = new Scanner(getClass.getResourceAsStream("/myfile.txt"))
    try {
      while (in.hasNext()) {
        words(in.next()) = words.getOrElse(in.next(), 0) + 1
      }
    } finally {
      in.close()
    }
    words
  }

  /**
    * ex 6
    *
    * Define a linked hash map that maps "Monday" to java.util.Calendar.MONDAY, and similarly for the other weekdays.
    * Demonstrate that the elements are visited in insertion order.
    *
    * @return
    */
  def weekdaysLinkedHashMap(): mutable.Map[String, Int] = {
    import java.util.Calendar._
    val days = scala.collection.mutable.LinkedHashMap(
      "MONDAY" -> MONDAY,
      "TUESDAY" -> TUESDAY,
      "WEDNESDAY" -> WEDNESDAY,
      "THURSDAY" -> THURSDAY,
      "FRIDAY" -> FRIDAY,
      "SATURDAY" -> SATURDAY,
      "SUNDAY" -> SUNDAY
    )

    days
  }


  /**
    * Print a table of all Java properties reported by the getProperties method of the java.lang.System
    * class, like this:
    * 53
    * java.runtime.name
    * sun.boot.library.path
    * java.vm.version
    * java.vm.vendor
    * java.vendor.url
    * path.separator
    * java.vm.name
    * | Java(TM) SE Runtime Environment
    * | /home/apps/jdk1.6.0_21/jre/lib/i386 | 17.0-b16
    * | Sun Microsystems Inc.
    * | http://java.sun.com/
    * |:
    * | Java HotSpot(TM) Server VM
    *
    * @return
    */
  def formatJavaProperties(): List[String] = {
    import scala.collection.JavaConversions.propertiesAsScalaMap
    val props = propertiesAsScalaMap(System.getProperties)
    val maxKeyLen = props.keySet.map(_.length).max

    val result = ListBuffer[String]()
    for ((key, value) <- props) {
      result += key.padTo(maxKeyLen, ' ') + " | " + value
    }

    result.toList
  }

  /**
    *
    * ex 8
    *
    * Write a function minmax(values: Array[Int]) that returns a pair
    * containing the smallest and the largest values in the array.
    *
    * @param values
    * @return
    */
  def minmax(values: Array[Int]): (Int, Int) = (values.min, values.max)


  /**
    * ex 9
    *
    * Write a function lteqgt(values: Array[Int], v: Int) that returns a triple containing the
    * counts of values less than v, equal to v, and greater than v.
    *
    * @param values
    * @param v
    * @return
    */
  def lteqgt(values: Array[Int], v: Int): (Int, Int, Int) = {
    (values.count(_ < v), values.count(_ == v), values.count(_ > v))
  }

  def prn(x: TraversableOnce[_]) =
    println(x.mkString(x.getClass.getSimpleName + "(", ", ", ")"))

  def time[R](block: => R): R = {
    val t0 = System.nanoTime()
    val result = block
    val t1 = System.nanoTime()
    println(" Elpased time : " + (t1 - t0) + "ns")
    println(" Elpased time : " + (t1 - t0) / 1000000 + "ms")
    result
  }

  def processWords(process: String => Unit): Unit = {
    val in = new Scanner(getClass.getResourceAsStream("/myfile.txt"))
    try {
      while (in.hasNext()) {
        process(in.next())
      }
    } finally {
      in.close()
    }

  }

  def main(args: Array[String]): Unit = {
    println("test1: mapValues==============")
    prn(time(reducedPrices()))
    println("test2: for yield===============")
    prn(time(reducedPrices2()))
    println("test3 : mutable map===============")
    time(countWordsInFile())
    println("test3 : mutable map===============")
    time(countWordsInFile2())
    println("test4 : immutable map===============")
    time(countWordsWithImutableMap())
    println("test4 : immutable map===============")
    time(countWordsWithImutableMap2())
    println("test5 : mutable sorted map===============")
    time(countWordsWithSortedMap())
    println("test6: immutable sorted map===============")
    time(countWordsWithImutabeSortedMap())

    println("test7: mutable tree map===============")
    time(countWordsWithTreeMap())

    println("test8: immutable tree map===============")
    time(countWordsWithImutabeTreeMap())

    println("test9: print propers===============")
    val li = formatJavaProperties()
    for (s <- li) {
      println(s)
    }

  }

}
