package chapt5

import scala.beans.BeanProperty

object Exercises extends App {
  println("ex 1==========")
  val cnt = new Counter
  println(cnt.increment())
  println(cnt.increment())
  println(cnt.increment())
  println(cnt.increment())

  println("ex 2==========")

  val bank = new BankAccount
  bank.deposit(100)

  bank.withDraw(200) match {
    case Left(s) => println(s)
    case Right(i) => println(i)
  }
  println(bank.total)

  println("ex 3==============")
  val time = new Time(1, 30)

  println("Time is: " + time.hour + ":" + time.minute)

  println(time.before(new Time(2, 30)))
  println(time.before(new Time(1, 10)))

  println(time.before2(new Time(2, 30)))
  println(time.before2(new Time(1, 10)))

  println("ex 4==============")
  val time2 = new Time2(1, 30)
  println("Time is: " + time2.hour + ":" + time2.minute)

  println(time2.before(new Time2(2, 30)))
  println(time2.before(new Time2(1, 10)))

  println("ex 5==============")
  val st = new Student
  st.setId(124)
  st.setName("hello")
  println(st.id)
  println(st.name)

  val st2 = new Student2("hello", 123)
  println(st2.id)
  println(st2.name)
  st2.setName("world")
  st2.setId(345)
  println(st2.id)
  println(st2.name)

  println("ex 6==============")
  val p = new Person(5)
  println(p.age)

  val n = new Person(-5)
  println(n.age)

  println("ex 7==============")
  val p2 = new Person2("Bill Gates")
  println(p2)

  val p3 = new Person3("hello kitty")
  println(p3)

  println("ex 8==============")
  val car1 = new Car("honda", "civic", 1982, "12366")
  println(car1)
  val car2 = new Car("honda", "civic", 1982)
  println(car2)
  val car3 = new Car("honda", "civic", "12356")
  println(car3)
  val car4 = new Car("honda", "civic")
  println(car4)

  println("ex 10==============")
  val e = new Employee
  println(e)
  val e1 = new Employee2("abc", 10000)
  println(e1)
  val e2 = new Employee3("def", 200000)
  println(e2)

  /**
    * ex 1
    *
    * Improve the Counter class in Section 5.1, “Simple Classes and Parameterless Methods,”
    * on page 55 so that it doesn’t turn negative at Int.MaxValue.
    *
    */
  class Counter {
    private var value = 0 // You must initialize the field
    def increment(): Either[String, Int] = {
      //      if(value == Int.MaxValue){
      if (value == 3) {
        Left("hi, reach the sky already..")
      } else {
        value += 1
        Right(value)
      }
    } // Methods are public by default
    def current() = value
  }

  /**
    * ex 2
    * Write a class BankAccount with methods deposit and withdraw, and a read-only property balance.
    *
    */
  class BankAccount {
    private var sum = 0

    def deposit(amount: Int) = {
      sum += amount
    }

    def withDraw(amount: Int): Either[String, Int] = {
      if (amount > sum) {
        Left("dude, why not rob the bank...")
      } else {
        sum -= amount
        Right(amount)
      }
    }

    def total = sum
  }

  /**
    * Write a class Time with read-only properties hours and minutes and a method
    * before(other: Time): Boolean
    * that checks whether this time comes before the other.
    * A Time object should be constructed as new Time(hrs, min), where hrs is in
    * military time format (between 0 and 23).
    *
    * @param h
    * @param min
    */
  class Time(private val h: Int, private val min: Int) {

    def hour: Int = h

    def minute: Int = min

    def before(other: Time): Boolean = {
      if (h == other.h) {
        return min < other.min
      }
      h < other.h
    }

    def before2(other: Time): Boolean = h * 60 + min < other.h * 60 + other.min
  }

  /**
    *
    * Reimplement the Time class from the preceding exercise so that the internal representation is the number
    * of minutes since midnight (between 0 and 24 × 60 – 1). Do not change the public
    * interface. That is, client code should be unaffected by your change.
    *
    * @param h
    * @param min
    */
  class Time2(private val h: Int, private val min: Int) {
    private val mins: Int = h * 60 + min

    def hour: Int = h

    def minute: Int = min

    def before(other: Time2): Boolean = mins < other.mins
  }

  /**
    *
    * Make a class Student with read-write JavaBeans properties name (of type String) and id (of type Long).
    * What methods are generated? (Use javap to check.)
    * Can you call the JavaBeans getters and setters in Scala? Should you?
    */
  class Student {
    @BeanProperty
    var name: String = ""

    @BeanProperty
    var id: Long = 0
  }

  class Student2(@BeanProperty var name: String, @BeanProperty var id: Long)

  /**
    *
    * In the Person class of Section 5.1, “Simple Classes and Parameterless Methods,” on page 55,
    * provide a primary constructor that turns negative ages to 0.
    *
    * @param age
    */
  class Person(var age: Int = 0) {
    if (age < 0) age = 0
  }

  /**
    * Write a class Person with a primary constructor that accepts a string containing a first name,
    * a space, and a last name, such as new Person("Fred Smith"). Supply read-only properties
    * firstName and lastName. Should the primary constructor
    * parameter be a var, a val, or a plain parameter? Why?
    *
    * @param name
    */
  class Person2(name: String) {
    val firstName = name.split(" ")(0)
    val lastName = name.split(" ")(1)

    override def toString: String =
      "Person2(" + firstName + ", " + lastName + ")"
  }

  // primary constructor is a private one
  class Person3 private(names: Array[String]) {
    val firstName: String = names(0)
    val lastName: String = names(1)

    // second(auxiliary) constructor
    def this(name: String) {
      this(Person3.parsedName(name))
    }

    override def toString: String =
      "Person3(" + firstName + ", " + lastName + ")"
  }

  private object Person3 {
    def parsedName(name: String): Array[String] = {
      val names = name.split(" ")
      if (names.length < 2) {
        throw new IllegalArgumentException("shuld have firstName and lastName")
      }
      names
    }
  }

  /**
    *
    * Make a class Car with read-only properties for manufacturer, model name, and model year,
    * and a read-write property for the license plate. Supply four constructors.
    * All require the manufacturer and model name. Optionally, model year and license plate can
    * also be specified in the constructor. If not, the model year is set to -1 and the license
    * plate to the empty string. Which constructor are you choosing as the primary constructor? Why?
    *
    * @param manufacture
    * @param modelName
    * @param modelYear
    * @param licence
    */
  class Car(val manufacture: String,
            val modelName: String,
            val modelYear: Int = -1,
            val licence: String = "") {
    def this(manufacture: String, modelName: String, license: String) {
      this(manufacture, modelName, -1, license)
    }

    override def toString: String =
      "Car(" + manufacture + ", " + modelName + ", " + modelYear + ", " + licence + ")"
  }

  /**
    *
    * Consider the class
    *
    * class Employee(val name: String, var salary: Double) {
    * def this() { this("John Q. Public", 0.0) }
    * }
    *
    * Rewrite it to use explicit fields and a default primary constructor. Which form do you prefer? Why?
    *
    */
  class Employee {
    val name: String = "John Q. Pbulic"
    val salary: Double = 0.0

    override def toString: String = "Employee(" + name + ", " + salary + ")"
  }

  class Employee2 {
    private var _name = "John Q. Public"
    var salary = 0.0

    def this(n: String, s: Double) {
      this()
      _name = n;
      salary = s;
    }

    def name = _name // read-only property, but private var
    override def toString = "Employee2(%s, %f)".format(name, salary)
  }

  class Employee3(private var n: String, private var s: Double) {
    def name: String = n

    def salary: Double = s

    override def toString = "Employee3(%s, %f)".format(name, salary)

  }

}
