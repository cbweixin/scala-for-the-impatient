package chapt6

object Exercises extends App {

  println("ex 1 ============")
  println(Conversion.gallonsToLitters(3))
  println(Conversion.inchesToCentimeters(3))
  println(Conversion.milesToKilometers(3))

  println("ex 2 ============")
  println(InchesToCentimeters.convert(3))
  println(MilesToKilometers.convert(3))
  println(GallonsToLitters.convert(3))

  println(InchesToCentimeters2.convert(3))
  println(MilesToKilometrs2.convert(3))
  println(GallonsToLitters2.convert(3))

  println("ex 3 ============")
  Origin.setLocation(1, 2)
  println(Origin.x + ", " + Origin.y)

  println("ex 4 ============")
  val p1 = Point(1, 2)
  println(p1)
  val p2 = Point(2, 3)
  println(p2)


  println("ex 6 ============")
  println(Cards.Clubs)
  println(Cards.Spades)
  println(Cards.Hearts)
  println(Cards.Diamonds)
  println(Cards.values)

  println("ex 7 ============")
  println(Cards.isRed(Cards.Spades))

  println("ex 8 =======")
  for (c <- RGBCube.values) println("0x%06x: %s".format(c.id, c))
  /**
    * ex 1
    * Write an object Conversions with methods inchesToCentimeters, gallonsToLiters,
    * and milesToKilometers.
    *
    */
  object Conversion {
    def inchesToCentimeters(inches: Double): Double = inches * 2.54

    def gallonsToLitters(gallons: Double): Double = gallons * 3.785

    def milesToKilometers(miles: Double): Double = miles / 0.62137
  }

  /**
    * ex 2
    *
    * The preceding problem wasn’t very object-oriented. Provide a general super- class UnitConversion and define
    * objects InchesToCentimeters, GallonsToLiters, and MilesToKilometers that extend it.
    *
    */
  abstract class UnitConversion {
    def convert(value: Double): Double
  }

  object InchesToCentimeters extends UnitConversion {
    override def convert(value: Double): Double = value * 2.54
  }

  object GallonsToLitters extends UnitConversion {
    override def convert(value: Double): Double = value * 3.785
  }

  object MilesToKilometers extends UnitConversion {
    override def convert(value: Double): Double = value / 0.62137
  }

  /**
    * ex 2
    *
    * @param factor
    */
  abstract class UnitConversion2(val factor: Double) {
    def convert(value: Double): Double = factor * value
  }

  object InchesToCentimeters2 extends UnitConversion2(2.54)

  object GallonsToLitters2 extends UnitConversion2(3.785)

  object MilesToKilometrs2 extends UnitConversion2(1.609)

  /**
    * ex 3
    *
    * Define an Origin object that extends java.awt.Point. Why is this not actually a good idea?
    * (Have a close look at the methods of the Point class.)
    */
  object Origin extends java.awt.Point {
    // we are not able to get an Origin instance by using `object` to extends Point
  }

  /**
    * ex 4
    * Define a Point class with a companion object so that you can construct Point
    * instances as Point(3, 4), without using new.
    *
    * @param x
    * @param y
    */
  class Point(x: Int, y: Int) extends java.awt.Point(x, y) {}

  object Point {
    def apply(x: Int, y: Int): Point = new Point(x, y)
  }

  /**
    * Error:(86, 32) overriding variable y in class Point of type Int;
    *
    * value y needs `override` modifier
    * class Point2(val x: Int, val y: Int) extends java.awt.Point(x, y) {}
    *
    */
  //  class Point2(val x: Int, val y: Int) extends java.awt.Point(x, y) {}

  /**
    * ex 5:
    * Write a Scala application, using the App trait, that prints the command-line
    * arguments in reverse order, separated by spaces. For example:
    * <blockquote><code>
    * scala Reverse Hello World
    * </code></blockquote>
    * should print
    * <blockquote><code>
    * World Hello
    * </code></blockquote>
    */
  object Reverse extends App {
    println(args.reverse.mkString(" "))
  }

  /**
    * ex 6
    * Write an enumeration describing the four playing card suits so that the toString
    * method returns ß, ®, ©, or TM.
    *
    */
  object Cards extends Enumeration {
    type Cards = Value
    val Clubs = Value("♣")
    val Diamonds = Value("♦")
    val Hearts = Value("♥")
    val Spades = Value("♠")


    /**
      * ex 7
      *
      * @param card
      * @return
      */
    def isRed(card: Cards): Boolean = card == Hearts || card == Diamonds

  }


  /**
    * ex 8
    *
    * Write an enumeration describing the eight corners of the RGB color
    * cube. As IDs, use the color values (for example, 0xff0000 for Red).
    */
  object RGBCube extends Enumeration {
    val black = Value(0x000000, "Black")
    val red = Value(0xff0000, "Red")
    val green = Value(0x00ff00, "Green")
    val blue = Value(0x0000ff, "Blue")
    val yellow = Value(0xffff00, "Yellow")
    val magenta = Value(0xff00ff, "Magenta")
    val cyan = Value(0x00ffff, "Cyan")
    val white = Value(0xffffff, "White")
  }


}
