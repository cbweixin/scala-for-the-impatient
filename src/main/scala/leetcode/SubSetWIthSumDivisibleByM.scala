package leetcode

// https://www.geeksforgeeks.org/subset-sum-divisible-m/
// given an array with integers, find out if there is a subset has sum % M == 0
object SubSetWIthSumDivisibleByM extends App {

  println(modularSum(Array(1, 7, 2), 5))
  println(modularSum(Array(1, 7), 5))
  println(modularSum(Array(1, 7, 3), 5))
  println(modularSum(Array(1, 7, 4, 2, 4), 5))

  //   DP problem
  // dp[i] = true|false, i is the modular of M, i <- 0 unitl M
  // also we do have dp[ (i + arr(k)) % M ] = dp[i]
  def modularSum(arr: Array[Int], M: Int): Boolean = {
    var dp: Array[Boolean] = Array.fill(M)(false)
    //    for (n <- arr) {
    //      dp(n % M) = true
    //    }
    //
    //    if (dp(0)) {
    //      return true
    //    }

    var tmp: Array[Boolean] = Array.fill(M)(false)
    arr.foreach { x =>
      {
        if (dp(0)) {
          return true
        }
        for (j <- 0 until M) {
          if (dp(j)) {
            if (!dp((j + x) % M)) {
              tmp((j + x)   % M) = true
            }
          }
        }
        tmp.zipWithIndex.foreach {
          case (v: Boolean, idx: Int) => {
            if (v) {
              dp(idx) = true
            }
          }
        }

        dp(x % M) = true
      }
    }

    return dp(0)
  }
}
