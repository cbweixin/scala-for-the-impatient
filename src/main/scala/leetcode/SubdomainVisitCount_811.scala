package leetcode

import scala.collection.mutable

object SubdomainVisitCount_811 extends App {

  Solution3.subdomainVisits(Array("9001 discuss.leetcode.com", "1000 yahoo.com")).foreach(println)

  def subdomainVisits(cpdomains: Array[String]): List[String] = {

    // notice how to intialize a map with default value
    val mMap = new mutable.HashMap[String, Int]().withDefaultValue(0)

    def process(str: String): Unit = {
      val ss: Array[String] = str.split(" ")
      val visit: Int = ss(0).toInt
      val domains = ss(1)
      // notice , for special character ".", we need to escape, both "\\." or "[.]" are working
      //      val frags: Array[String] = domains.split("[.]")
      val frags: Array[String] = domains.split("\\.")
      for (i <- 0 until frags.length) {
        val key = frags.slice(i, frags.length).mkString(".")
        // notice the update usage
        mMap.update(key, mMap(key) + visit)
      }
    }

    cpdomains.foreach(process)

    var res = List[String]()
    for ((k, v) <- mMap) {
      res = res :+ v.toString.concat(" " + k)
    }

    return res

  }

  object Solution {

    def subdomainVisits(cpdomains: Array[String]): List[String] = {

      // test case 999 discuss.leetcode.com
      // i is Array("999", "discuss.leetcode.com")
      // x is 1->3, i(1) is "discuss.leetcode.com", takeRight(1) -> com, takeRight(2)->leetcode.com, takeRight(3) ->
      // discuss.leetcode.com
      // so (j, i(0)) would be a tuple like (com, 999), (leetcode.com, 999), etc
      val domain_tuples = for (i <- cpdomains.map(x => x.split(" "));
                               j <- (1 to i(1).split('.').length).map(x => i(1).split('.').takeRight(x).mkString(".")))
        yield (j, i(0))

      // var result = domain_tuples.groupBy(_._1).mapValues(group => group.map(_._2.toInt).sum)

      // Another way to sum by key
      // domain_tuples is something like this Array(("com",999), ("yahoo.com",899), ("com",1))
      // we groupBy(_._1), in scala, for a tuple, _._1 is the first item, _._2 is the second, after group
      // we got scala.collection.immutable.Map[String,Array[(String, Int)]] = Map(yahoo.com -> Array((yahoo.com,899)),
      // com -> Array((com,999), (com,1)))
      // so xs is an Array of tuple, then `com` would grouped together, we would got
      // Array(("com", 1000), ("yahoo.com", 1))
      var result = for { (key, xs) <- domain_tuples.groupBy(_._1) } yield (key, xs.map(_._2.toInt).sum)

      // the tuple in Array would become a string, then change it to list
      result.map(x => x._2.toString + " " + x._1).toList

    }
  }

  object Solution2 {

    def subdomainVisits(cpdomains: Array[String]): List[String] = {
      var domainMap = Map.empty[String, Int]

      def addToDomMap(str: String, toAdd: Int): Unit = {
        domainMap.get(str) match {
          case Some(current) => domainMap = domainMap + (str -> (current + toAdd))
          case None => domainMap = domainMap + (str -> toAdd)
        }
      }

      cpdomains foreach { domStr =>
        val split = domStr.split(" ")
        // wholeDOm : discuss, leetcode, com
        val wholeDom = split(1).split('.')

        val hits = split(0).toInt
        if (hits > 0) {
          // notice the usage of foldLeft
          wholeDom.reverse.foldLeft(List.empty[String]) {
            case (list, domStr) =>
              if (list.isEmpty) {
                addToDomMap(domStr, hits)
                list.+:(domStr)
              } else {
                val newStr = s"$domStr.${list.head}"
                addToDomMap(newStr, hits)
                list.+:(newStr)
              }
          }
        }
      }

      domainMap.toList.map({
        case (key, value) =>
          s"$value $key"
      })
    }
  }

  object Solution3 {

    def subdomainVisits(cpdomains: Array[String]): List[String] = {
      def subdomains(d: String): Seq[String] = {
        var s = d

        val b = new scala.collection.mutable.ArrayBuffer[String]()
        b += s
        var i = s.indexOf('.')
        while (i >= 0 && i < s.length - 1) {
          s = s.substring(i + 1)
          b += s
          i = s.indexOf('.')
        }
        b
      }

      def parse(s: String): Seq[(String, Int)] = s.split("\\s") match {
        case Array(count, domain) => subdomains(domain).map(x => (x, count.toInt))
        case _ => throw new IllegalArgumentException
      }

      //      val ss1 = cpdomains.map(parse)
      //      val ss = cpdomains.flatMap(parse)

      // why we need a flatMap here? watch the `parse` function carefully, it would generate a 2-dimension array like
      // this way [[("com", 9001),("leetcode.com", 9001), ("discuss.leetcode.com", 9001)], [("com", 1000), ("yahoo
      // .com",1000)]]
      // after flatMap, it would becomes [ ("com", 9001), ("com", 1000)..], etc
      cpdomains
        .flatMap(parse)
        .groupBy(_._1)
        .mapValues(x => x.foldLeft(0)((acc, x) => acc + x._2))
        .toList
        .map(x => s"${x._2} ${x._1}")
    }
  }

  object Solution4 {

    import scala.collection.mutable

    def subdomainVisits(cpdomains: Array[String]): List[String] = {
      val domainsCounts = mutable.HashMap[String, Int]()
      cpdomains
        .map(_.split(" "))
        .foreach(ar => {
          val count = ar(0).toInt
          val domain = ar(1)
          domainsCounts +=
            domain -> (domainsCounts.getOrElse(domain, 0) + count)
          for (i <- 1 until domain.length) {
            if (domain(i) == '.') {
              val webSite = domain.substring(i + 1)
              domainsCounts +=
                webSite -> (domainsCounts.getOrElse(webSite, 0) + count)
            }
          }
        })
      domainsCounts
        .map(p => "" + p._2 + " " + p._1)
        .toList
    }
  }
}
