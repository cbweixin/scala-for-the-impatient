package leetcode

object GasStation_134 extends App {

  def canCompleteCircuit(gas: Array[Int], cost: Array[Int]): Int = {

    var total = 0
    var sum = 0
    var start = 0

    for (i <- 0 until gas.length) {
      total += gas(i) - cost(i)
      sum += gas(i) - cost(i)
      if (sum < 0) {
        sum = 0
        start = i + 1
      }

    }

    if (total < 0) {
      return -1
    }

    return start

  }
}
