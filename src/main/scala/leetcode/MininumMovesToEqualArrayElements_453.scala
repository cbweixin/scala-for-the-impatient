package leetcode

object MininumMovesToEqualArrayElements_453 extends App {

  def minMoves(nums: Array[Int]): Int = {
    var steps = 0
    val m = nums.min
    nums.foreach(n => steps += (n - m))
    steps
  }

  def minMoves2(nums: Array[Int]): Int = {
    val m = nums.min
    nums.map(_ - m).sum
  }
}
