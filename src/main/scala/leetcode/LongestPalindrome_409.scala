package leetcode

object LongestPalindrome_409 {

  def longestPalindrome(s: String): Int = {
    val counter = s.groupBy(identity).mapValues(_.size)
    val odds = (for ((k, v) <- counter if ((v & 0x1) > 0)) yield 1).sum

    s.length - odds + (if (odds > 0) 1 else 0)

  }

  object Solution {

    def longestPalindrome(s: String): Int = {
      val map = Array.ofDim[Boolean](128)

      var res = 0
      for (c <- s) {
        map(c) = !map(c)
        if (!map(c)) res += 2 // 每经历了2次就回到初始状态,此时+2
      }
      if (res < s.length) res += 1 // 说明有奇数次的存在,其回不到初始状态,此时可以将其中一个安排在`aba`的b

      res
    }
  }

  object Solution2 {

    def longestPalindrome(s: String): Int = {
      val charCounts = s.groupBy(c => c).values.map(_.length)
      val evenCharLength = charCounts.map(l => l - (l % 2)).sum
      val hasEven = charCounts.find(_ % 2 == 1).isDefined
      evenCharLength + (if (hasEven) 1 else 0)
    }
  }
}
