package leetcode

object MedianOf2SortedArray_4 extends App {

  println(findMedianSortedArrays(Array(1, 3), Array(2)))
  println(findMedianSortedArrays(Array(1, 2), Array(3, 4)))

  def findMedianSortedArrays(nums1: Array[Int], nums2: Array[Int]): Double = {

    def getKth(a1: Array[Int], a2: Array[Int], k: Int): Double = {
      if (a1.length > a2.length) {
        return getKth(a2, a1, k)
      }
      if (a1.length == 0) {
        return a2(k - 1).toDouble
      }
      if (k == 1) {
        return math.min(a1(0), a2(0)).toDouble
      }
      val i = math.min(a1.length, k / 2)
      val j = math.min(a2.length, k / 2)
      if (a1(i - 1) < a2(j - 1)) {
        return getKth(a1.slice(i, a1.length), a2, k - i)
      }

      return getKth(a1, a2.slice(j, a2.length), k - j)
    }

    val (m, n) = (nums1.length, nums2.length)
    (getKth(nums1, nums2, (m + n + 1) / 2) + getKth(nums1, nums2, (m + n + 2) / 2)) / 2

  }
}
