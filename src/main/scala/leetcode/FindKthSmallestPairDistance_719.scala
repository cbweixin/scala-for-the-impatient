package leetcode

object FindKthSmallestPairDistance_719 extends App {
  println(smallestDistancePair(Array(1, 6, 1), 3))
  println(smallestDistancePair(Array(1, 3, 1), 1))

  def smallestDistancePair(nums: Array[Int], k: Int): Int = {
    val sorted = nums.sorted
    val maxDist = sorted.last - sorted.head
    // how to define multiple vars in one line, like python
    var (left, right, start) = (0, maxDist, 0)

    while (left < right) {
      val mid = left + (right - left) / 2
      var s = 0
      start = 0
      for (i <- 0 until sorted.length) {
        while (start < i && sorted(i) - sorted(start) > mid) {
          start += 1
        }
        s += i - start
      }
      s match {
        case l if (l >= k) => right = mid
        case _ => left = mid + 1
      }
    }
    right
  }
}
