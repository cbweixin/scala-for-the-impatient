package leetcode

object BasicCalculatorII_227 extends App {

  println(calculate("3+2*2"))
  println(calculate(" 3/2 "))
  println(calculate(" 3+5 / 2 "))

  def calculate(s: String): Int = {
    val operands = scala.collection.mutable.ArrayStack[Int]()
    val ss = s + "+" // for calc, 3 + 2 * 2 , make it 3 + 2 * 2 +, so no need to do check if we reach the end or not
    var sign = '+'
    val operand = new StringBuilder
    for (c <- ss) {
      c match {
        case n if n.isDigit => operand.append(n)
        case b if b.isWhitespace => // notice the empty branch case, we do nothing
        case x => {
          val temp = operand.toInt
          operand.clear()
          sign match {
            case '+' => operands.push(temp)
            case '-' => operands.push(-temp)
            case '*' => operands.push(operands.pop * temp)
            case '/' => operands.push(operands.pop / temp)
          }
          sign = x
        }
      }
    }

    var res = 0
    while (!operands.isEmpty) {
      res += operands.pop
    }
    res
  }

  object Solution {

    import scala.collection.mutable

    def calculate(pinput: String): Int = {
      var stack = List.empty[Int]

      val ac = mutable.ListBuffer.empty[Char]
      var prevOp = '+'
      val ops = List('+', '-', '*', '/')
      val input = pinput.trim // 去掉头尾空格，方便判断最后一次数字结束的位置

      for (i <- input.indices if input(i) != ' ') {
        val curr = input(i)
        if (!ops.contains(curr))
          ac += curr
        if (ops.contains(curr) || i == input.length - 1) { // 此处增加的判断是为了处理最后一个op和数字，不必循环外再重写一遍下面的逻辑
          val acNum = ac.mkString("").toInt
          prevOp match {
            case '+' =>
              stack = acNum +: stack
            case '-' =>
              stack = acNum * -1 +: stack
            case '*' =>
              val res = stack.head * acNum
              stack = res +: stack.tail
            case '/' =>
              val res = stack.head / acNum
              stack = res +: stack.tail
          }
          ac.clear
          prevOp = curr
        }
      }

      stack.sum
    }
  }

}
