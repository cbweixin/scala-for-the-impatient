package leetcode

object PowerfulIntegers_970 extends App {
  //  println(powerfulIntegers(2, 4, 12))
  println(powerfulIntegers(50, 40, 10000))

  def powerfulIntegers(x: Int, y: Int, bound: Int): List[Int] = {

    var res = List[Int]()

    if (bound > 0) {
      if (x == 1 && y == 1) {
        if (bound >= 2) {
          res = res :+ 2
        }
      } else if (x > y) {
        // for recursive call, you have to return
        return powerfulIntegers(y, x, bound)
      } else if (x == 1) {
        for (j <- 0 to (scala.math.log10(bound) / scala.math.log10(y)).toInt + 1) {
          val s = x + scala.math.pow(y, j).toInt
          if (s <= bound && !res.contains(s)) {
            res = res :+ s
          }
        }
      } else {
        for (i <- 0 to (scala.math.log10(bound) / scala.math.log10(x)).toInt) {
          for (j <- 0 to (scala.math.log10(bound) / scala.math.log10(y)).toInt) {
            val s = (scala.math.pow(x, i) + scala.math.pow(y, j)).toInt
            if (s <= bound && !res.contains(s)) {
              res = res :+ s
            }

          }
        }
      }

    }
    res
  }
}
