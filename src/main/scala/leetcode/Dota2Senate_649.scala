package leetcode

object Dota2Senate_649 extends App {

  println(predictPartyVictory("RD"))
  println(predictPartyVictory("RDD"))

  def predictPartyVictory(senate: String): String = {
    val dq = scala.collection.mutable.Queue[Int]()
    val rq = scala.collection.mutable.Queue[Int]()
    val l = senate.size
    for (i <- 0 until senate.length) {
      senate(i) match {
        case 'D' => dq += i
        case 'R' => rq += i
      }

    }

    while (dq.size > 0 && rq.size > 0) {
      val d = dq.dequeue()
      val r = rq.dequeue()
      if (d < r) {
        dq += d + l
      } else {
        rq += r + l
      }
    }

    if (dq.size > 0) {
      "Dire"
    } else {
      "Radiant"
    }
  }
}
