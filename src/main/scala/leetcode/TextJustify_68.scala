package leetcode

import scala.collection.mutable.ListBuffer

object TextJustify_68 extends App {

  fullJustify(Array("What", "must", "be", "acknowledgment", "shall", "be"), 16).foreach(println)
  fullJustify(Array("This", "is", "an", "example", "of", "text", "justification."), 16).foreach(println)

  def fullJustify(words: Array[String], maxWidth: Int): List[String] = {
    val res: ListBuffer[String] = ListBuffer()
    val cur: ListBuffer[String] = ListBuffer()
    var numLetter = 0

    for (w <- words) {
      // if the newly added word w makes the line over maxWidth
      // notice how we calculate the length
      // cur.length means the space #
      if (w.length + numLetter + cur.length > maxWidth) {
        val l = math.max(cur.length - 1, 1)
        for (i <- 0 until maxWidth - numLetter) {
          cur(i % l) = cur(i % l) + " "
        }
        res += cur.mkString("")
        cur.clear()
        numLetter = 0
      }
      cur += w
      numLetter += w.length
    }

    var s = cur.mkString(" ")
    // in scala, you can quickly multiply a string, like python.
    val t = " " * (maxWidth - s.length)
    s += t

    res += s
    res.toList
  }
}
