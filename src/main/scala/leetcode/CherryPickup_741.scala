package leetcode

import scala.collection.mutable

object CherryPickup_741 extends App {

  println(cherryPickup(Array(Array(0, 1, -1), Array(1, 0, -1), Array(1, 1, 1))))

  def cherryPickup(grid: Array[Array[Int]]): Int = {
    val posMap = mutable.HashMap.empty[(Int, Int, Int), Int]
    def dfs(x: Int, y: Int, z: Int): Int = {
      if (posMap.contains((x, y, z))) {
        return posMap.get((x, y, z)).getOrElse(0)
      }
      if (x == 0 && y == 0 && z == 0) {
        posMap((x, y, z)) = grid(x)(y)
        return grid(x)(y)
      }
      if (x < 0 || y < 0 || z < 0 || x + y - z < 0) {
        return -1
      }

      if (grid(x)(y) < 0 || grid(z)(x + y - z) < 0) {
        return -1
      }

      var m = List(dfs(x - 1, y, z), dfs(x - 1, y, z - 1), dfs(x, y - 1, z - 1), dfs(x, y - 1, z)).sorted.last
      if (m < 0) {
        posMap((x, y, z)) = -1
        return -1
      }
      if (x == z) {
        m += grid(x)(y)
      } else {
        m += grid(x)(y) + grid(z)(x + y - z)
      }
      posMap((x, y, z)) = m
      return m
    }
    val N = grid.length
    return math.max(0, dfs(N - 1, N - 1, N - 1))
  }

  object Solution {

    var dp: Array[Array[Array[Int]]] = null

    def getMax(a: Int, b: Int, c: Int, d: Int): Int = {
      return Math.max(Math.max(a, b), Math.max(c, d))
    }

    // split to 2 trips: 0,0 --> i,j, and 0,0 --> p,q
    // as long as i, j don't show up in previous paths of p,q and vice versa, we are good
    // i+j = p+q = step is the way to guarantee this
    // for cases that i+j != p+q but still i,j outside p,q, it will be evetually covered by later "steps"
    // each step refines the result
    def cherryPickup(grid: Array[Array[Int]]): Int = {
      val rowLimit = grid.length
      val colLimit = grid(0).length
      val n = grid.length
      val overallStep = 2 * n - 2
      dp = Array.fill(overallStep + 1, rowLimit, colLimit)(0)
      dp(0)(0)(0) = grid(0)(0)
      var overallMax = dp(0)(n - 1)(n - 1)
      for (step <- 1 to overallStep) {
        for (i <- 0 until n) {
          for (p <- 0 until n) {
            val j = step - i
            val q = step - p
            if (j < 0 || q < 0 || j >= n || q >= n || grid(i)(j) == -1 || grid(p)(q) == -1) {
              dp(step)(i)(p) = Int.MinValue
            } else {
              var max = 0
              // step-1, i and i+j == step means j-1
              if (i >= 1 && p >= 1) {
                max =
                  getMax(dp(step - 1)(i - 1)(p), dp(step - 1)(i)(p), dp(step - 1)(i - 1)(p - 1), dp(step - 1)(i)(p - 1))
              } else if (p >= 1) { // i==0
                max = Math.max(dp(step - 1)(i)(p), dp(step - 1)(i)(p - 1))
              } else if (i >= 1) { // p==0
                max = Math.max(dp(step - 1)(i - 1)(p), dp(step - 1)(i)(p))
              } else {
                max = dp(step - 1)(i)(p)
              }
              val cur = grid(i)(j) + {
                if (i == p) 0 else grid(p)(q)
              }
              max += cur
              dp(step)(i)(p) = max
            }
          }
        }

      }
      // full step must be 2n-2 so that i and j are both n-1
      overallMax = Math.max(overallMax, dp(overallStep)(n - 1)(n - 1))
      return overallMax
    }
  }
}
