package leetcode

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object FindKpairsWithSmallestSums_373 extends App {

  /*  kSmallestPairs(Array(1, 7, 11), Array(2, 4, 6), 3).map(v => {
    print(s"(${v(0)}, ${v(1)}) ")
  })

  println("------")*/

  kSmallestPairs(Array(1, 1, 2), Array(1, 2, 3), 10).map(v => {
    print(s"(${v(0)}, ${v(1)}) ")
  })

  def kSmallestPairs(nums1: Array[Int], nums2: Array[Int], k: Int): List[List[Int]] = {

    val res = ListBuffer[List[Int]]()

    val pq = mutable.PriorityQueue[(Int, Int, Int)]()(Ordering.by {
      // in scala, by default, PQ is a max heap
      // first compare times, sort the appear time ascending, then compare string, sort the string descending
      // notice the "-t" ordering, the word appear few times, or the word alphabetical greater, woudl be on top
      // and deque firstly
      //      case (v, s, t) => (-v, s, t)
      case (v, s, t) => -v
    })

    def push(i: Int, j: Int) = {
      if (i < nums1.length && j < nums2.length) {
        pq += ((nums1(i) + nums2(j), i, j))
      }
    }

    push(0, 0)

    while (!pq.isEmpty && res.length < k) {
      val (v, i, j) = pq.dequeue()
      res += List(nums1(i), nums2(j))
      //      res += List(i, j)
      push(i, j + 1)
      if (j == 0) {
        push(i + 1, j)
      }
    }

    res.toList

  }

  object Solution {

    def kSmallestPairs(nums1: Array[Int], nums2: Array[Int], k: Int): List[Array[Int]] = {
      val res = ListBuffer[Array[Int]]()
      if (nums1 == null || nums1.isEmpty || nums2 == null || nums2.isEmpty) return res.toList

      val pq = mutable.PriorityQueue[((Int, Int), Int)]()(Ordering.by(e => -e._2)) // asc, [coord(i,j), sum]
      for (j <- nums2.indices) pq.enqueue(((0, j), nums1(0) + nums2(j))) // 初始化,以i=0这一段开始
      for (_ <- 0 until math.min(k, nums1.length * nums2.length)) { // 总的组合个数
        val cur = pq.dequeue()
        val (x, y) = (cur._1._1, cur._1._2)
        res.append(Array(nums1(x), nums2(y)))
        if (x < nums1.length - 1) pq.enqueue(((x + 1, y), nums1(x + 1) + nums2(y)))
      }

      res.toList
    }
  }

  object Solution2 {

    def kSmallestPairs(nums1: Array[Int], nums2: Array[Int], k: Int): List[Array[Int]] = {
      if (nums1.isEmpty || nums2.isEmpty) return Nil
      val result = new ListBuffer[Array[Int]]()
      val pq = new mutable.PriorityQueue[(Int, Int, Int)]()(Ordering.by(-_._1))
      pq ++= (0 until nums1.length).map(i => (nums1(i) + nums2.head, i, 0))
      for (t <- 1 to scala.math.min(k, nums1.length * nums2.length)) {
        val (s, i, j0) = pq.dequeue
        result += Array(nums1(i), nums2(j0))
        val j = j0 + 1
        if (j < nums2.length) pq += ((nums1(i) + nums2(j), i, j))
      }
      result.toList
    }
  }
}
