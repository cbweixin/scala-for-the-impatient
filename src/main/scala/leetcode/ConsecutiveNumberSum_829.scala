package leetcode

object ConsecutiveNumberSum_829 extends App {
  println(consecutiveNumbersSum2(2))
  println(consecutiveNumbersSum2(5))
  println(consecutiveNumbersSum2(15))
  println(consecutiveNumbersSum(15))

  def consecutiveNumbersSum(N: Int): Int = {
    var res = 1
    for (i <- 2 to math.sqrt(2 * N).toInt) {
      if ((N - i * (i - 1) / 2) % i == 0) {
        res += 1
      }
    }

    res
  }

  // more concise
  def consecutiveNumbersSum2(N: Int): Int = {
    (for (i <- 2 to math.sqrt(2 * N).toInt if ((N - i * (i - 1) / 2) % i == 0)) yield 1).sum + 1
  }
}
