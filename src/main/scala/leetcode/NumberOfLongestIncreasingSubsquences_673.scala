package leetcode

object NumberOfLongestIncreasingSubsquences_673 extends App {

  println(findNumberOfLIS(Array(1, 3, 5, 4, 7)))

  def findNumberOfLIS(nums: Array[Int]): Int = {
    if (nums.isEmpty) {
      return 0
    }
    val N = nums length
    val LIS = Array.fill(N)(1)
    val COUNT = Array.fill(N)(1)
    var res = 0
    var maxL = 1

    for (i <- 0 until N) {
      for (j <- 0 until i) {
        if (nums(i) > nums(j)) {
          if (LIS(i) == LIS(j) + 1) {
            COUNT(i) += COUNT(j)
          } else if (LIS(i) < LIS(j) + 1) {
            LIS(i) = LIS(j) + 1
            COUNT(i) = COUNT(j)
          }
        }
      }
      if (maxL == LIS(i)) {
        res += COUNT(i)
      } else if (maxL < LIS(i)) {
        maxL = LIS(i)
        res = COUNT(i)
      }

    }
    res
  }
}
