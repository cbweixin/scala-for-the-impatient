package leetcode

object ThirdMaximumNumbers_414 extends App {
  println(thirdMax(Array(1, 2)))
  println(thirdMax(Array(2, 2, 3, 1)))
  println(thirdMax(Array(3, 2, 1)))
  println(thirdMax(Array(1, 2, -2147483648)))
  println(Solution3.thirdMax(Array(2, 2, 3, 1)))
  println(Solution3.thirdMax(Array(1, 2, -2147483648)))

  def thirdMax(nums: Array[Int]): Int = {
    var first, second, third = Long.MinValue

    for (n <- nums) {
      if (n > first) {
        third = second
        second = first
        first = n
      } else if (n > second && n < first) {
        third = second
        second = n
      } else if (n > third && n < second) {
        third = n
      }
    }

    if (third == Long.MinValue) first.toInt else third.toInt

  }

  object Solution1 {

    def thirdMax(nums: Array[Int]): Int = {
      val pq = new scala.collection.mutable.PriorityQueue[Int]().reverse
      nums
        .toSet[Int]
        .foreach(x => {
          if (pq.size >= 3) {
            if (pq.head < x) {
              pq.dequeue()
              pq.enqueue(x)
            }
          } else
            pq.enqueue(x)
        })
      if (pq.size == 3)
        pq.head
      else
        pq.last
    }
  }

  object Solution2 {

    def thirdMax(nums: Array[Int]): Int = {
      import scala.collection.mutable
      val top3 = new mutable.TreeSet[Int]()
      nums.foreach(x => {
        top3 += x
        if (top3.size > 3) top3.remove(top3.head)
      })
      if (top3.size < 3) top3.max else top3.min
    }
  }

  object Solution3 {

    def thirdMax(nums: Array[Int]): Int = {
      val pq = scala.collection.mutable.PriorityQueue[Int](nums.distinct: _*)
      if (pq.size < 3) {
        pq.dequeue()
      } else {
        // for [1,2,3,4,5], tail => [2,3,4,5], tail again => [3,4,5]
        // so call tail twice you got the third max number
        pq.tail.tail.head
      }
    }
  }

  object Solution4 {

    def thirdMax(nums: Array[Int]): Int = {
      var l = List[Int]()

      nums.foreach(n => {
        l = (n :: l).distinct.sorted.takeRight(3)
      })

      if (l.size < 3) l.max else l.min
    }
  }

}
