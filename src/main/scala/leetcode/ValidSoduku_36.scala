package leetcode

import scala.collection.mutable

object ValidSoduku_36 extends App {

  def isValidSudoku(board: Array[Array[Char]]): Boolean = {
    def isValidList(li: Array[Char]): Boolean = {
      val l = li.filter(x => x != '.')
      l.distinct.length == l.length
    }

    for (i <- 0 until 9) {
      if (!isValidList(board(i))) {
        return false
      }
    }

    for (j <- 0 until 9) {
      // knowledget how to quickly get column from 2 -dim array
      if (!isValidList(board.map(_(j)))) {
        return false
      }
    }

    for (i <- 0 to 2; j <- 0 to 2) {
      val t = Array.ofDim[Char](9)
      var k = 0
      for (m <- 3 * i to 3 * i + 2; n <- 3 * j to 3 * j + 2) {
        t(k) = board(m)(n)
        k += 1
      }
      if (!isValidList(t)) {
        return false
      }
    }

    true
  }

  object Solution {
    private val BoardSize = 9

    def isValidSudoku(board: Array[Array[Char]]): Boolean = {
      val seen = mutable.Set.empty[String]
      for (i <- 0 until BoardSize) {
        for (j <- 0 until BoardSize) {
          val ch = board(i)(j)
          if (ch != '.') {
            if (!seen.add(ch + " row " + i) || !seen.add(ch + " col " + j) || !seen.add(ch + " grid " + i / 3 + " " + j / 3))
              return false
          }
        }
      }
      true
    }
}
