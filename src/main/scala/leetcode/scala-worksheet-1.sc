import scala.collection.mutable.ListBuffer

val a = ("abc", 1)
val b = ("abc" -> 1)
b._1
b._2
a._1
a._2
123 -> "def"

val c = StringContext treatEscapes ("[{\"realmId\":\"123148397371479\"}]")
val numbers = List(5, 4, 8, 6, 2)
numbers.fold(2)(_ + _)

class Foo(val name: String, val age: Int, val sex: Symbol)

object Foo {
  def apply(name: String, age: Int, sex: Symbol) = new Foo(name, age, sex)
}

val fooList = Foo("Hugh Jass", 25, 'male) ::
  Foo("Biggus Dickus", 43, 'male) ::
  Foo("Incontinentia Buttocks", 37, 'female) ::
  Nil


val stringList = fooList.foldLeft[List[String]](List[String]()) { (z, f) =>
  val title = f.sex match {
    case 'male => "Mr."
    case 'female => "Ms."
  }
  z :+ s"$title ${f.name}, ${f.age}"
}

val stringList2 = fooList.foldRight[List[String]](List[String]()) { (f, z) =>
  val title = f.sex match {
    case 'male => "Mr."
    case 'female => "Ms."
  }
  z :+ s"$title ${f.name}, ${f.age}"
}


val strs = Array("ab", "cd", "ef")
val sss = strs.mkString(" ", " ", "")

val listBuffer1: ListBuffer[String] = ListBuffer("Plain Donut", "Strawberry Donut", "Chocolate Donut")
listBuffer1 ++: List[String]("Glazed Donut", "Krispy creme")

val strs2 = Array("discuss", "leeetcode", "com")
val st1 = strs2.takeRight(3).mkString(".")

val groups = Array(("com", 999), ("yahoo.com", 899), ("com", 1))
val y = groups.groupBy(_._1)
groups.map(x => x._2.toString + " " + x._1)


var mmap = Map.empty[String, Int]
mmap = mmap + ("A" -> 1)
mmap = mmap + ("A" -> 2)

val lb = new ListBuffer[String]
lb += "a"
lb += "b"
lb(0) = lb(0) + "d"
lb += "f"

val s = "abc"
for (c <- s) {
  println(c.getClass)
}


val map1 = scala.collection.mutable.HashMap.empty[Tuple2[Int, Int], Int]
map1.put((1, 2), 3)
println(map1.get((1, 2)))


val map2 = scala.collection.mutable.HashMap.empty[(Int, Int), Int]
map2.put((1, 3), 5)
println(map2.get((1, 3)))

val map3 = scala.collection.mutable.HashMap.empty[(Int, Int, Int), Int]
map3.put((1, 3, 4), 5)
println(map3.get((1, 3, 4)))


// how to use variable length paramters
def echo(args: String*): Unit = {
  for (arg <- args) {
    println(arg)
  }
}

// how to pass in all elements of an array
val ab = Array("Hello", "world")
echo(ab: _*)


class O {
  def >(o: O): Boolean = {
    println("compare")
    true
  }
}

// by-name parameter, function evaluate inside the body
def byName(predicate: => Boolean) = {
  println("before predicate")
  predicate
  println("after predicate")
}

// by-value parameter, function already evaluated when passed in
def byValue(predicate: Boolean) = {
  println("before predicate")
  predicate
  println("after predicate")
}


// another good example :
// https://alvinalexander.com/source-code/scala/simple-scala-call-name-example
val o1 = new O
val o2 = new O
byName(o1 > o2)
byValue(o1 > o2)


(List("a", "b", "c") zip List("1", "2", "3")) map (x => x._1 + ":" + x._2)
(List("a", "b", "c") zip List("1", "2", "3")) map {
  case (a: String, b: String) => a + ":" + b
}


def smaller[T](a: T, b: T)(implicit order: T => Ordered[T]) =
  if (order(a) < b) a else b

smaller(40, 2)

def small[T](a: T, b: T)(implicit order: T => Ordered[T]) =
  if (a < b) a else b

small("hello", "world")

// long to hex
13690566117625L.toHexString

// long to binary
13690566117625L.toBinaryString

// also applied to int

42.toHexString

42.toBinaryString

// another way like python
// %X means uppercase, %x means lowercase
val bigNum: Long = 13690566117625L
val bigHex: String = f"$bigNum%X"
val smallHex: String = f"$bigNum%x"

// you can also padding, notice the difference between
// %016X and %16X. for %016X, it would use '0' to do padding
// otherwise, it use space(empty string) to do padding
val longhex: String = f"$bigNum%016X"
val longhex1: String = f"$bigNum%16X"
val ii = 1
val ii_hex: String = f"$ii%8X"
val ii_hex2: String = f"$ii%08X"


// string interpolation f vs s, f means format, like
// c printf, also f interpolation is typesafe.

val name = "John";
println(s"Hello $name")
println(f"Hello $name%2.4s")

// generic classes
class Pair[T, S](val first: T, val second: S)

val p1 = new Pair(42, "String")
println(s"${p1.first} : ${p1.second}")

val p2 = new Pair[Any, Any](34, "hi")
println(s"${p2.first} : ${p2.second}")

// generic functions

def getMiddle[T](a: Array[T]): T = a(a.length / 2)
getMiddle(Array("Marry", "had", "a", "little", "lamb"))


// Bounds for type variables
class Pair2[T](val first: T, val second: T) {
  //  below code is wrong, because T has no `compareTo` method
  //  def smaller = if(first.compareTo(second)<0) first else second
}

// so we need to add an UpperBoundType
class Pair3[T <: Comparable[T]](val first: T, val second: T) {
  def smaller = if (first.compareTo(second) < 0) first else second
}

val p3 = new Pair3[String]("fred", "brooks")
println(p3.smaller)

// got error : Error:(209, 13) type arguments [Int] do not conform to class Pair3's type parameter bounds [T <: Comparable[T]]
//val p4 = new Pair3[Int](4,2)
//println(p4.smaller)


// solution View bounds
// Unlike the java.lang.Integer wrapper type, the Scala Int
// type does not
// implement Comparable. However,
// RichInt does im- plement Comparable[Int], and there is
// an implicit conversion from Int to RichInt.
class Pair4[T <% Comparable[T]](val first: T, val second: T) {
  def smaller = if (first.compareTo(second) < 0) first else second
}

val p4 = new Pair4[Int](4, 2)
println(p4.smaller)

// using implicit also ok
class Pair5[T](val first: T, val second: T)(implicit ev: T => Comparable[T]) {
  def smaller = if (first.compareTo(second) < 0) first else second
}

val p5 = new Pair4[Int](4, 9)
println(p5.smaller)


// context bounds
//A context bound has the form T : M, where M is
// another generic type. It requires that there
// is an “implicit value” of type M[T]
//  When you declare a method that uses the implicit value,
//  you have to add an “implicit parameter.”

class Pair6[T: Ordering](val first: T, val second: T) {
  def smaller(implicit ord: Ordering[T]): T = {
    if (ord.compare(first, second) < 0) first else second
  }
}

val p6 = new Pair6[Int](10, 100)
println(p6.smaller)


// the ClassTag context bounds
import scala.reflect._

def makePair[T: ClassTag](first: T, second: T): Array[T] = {
  val r = new Array[T](2)
  r(0) = first
  r(1) = second
  r
}

//If you call makePair(4, 9), the compiler locates the implicit ClassTag[Int] and
// actually calls makePair(4, 9)(classTag). Then the new operator is translated
// to a call classTag.newArray, which in the case of a ClassTag[Int] constructs
// a primitive array int[2].
//val p7: Array[Int] = makePair(4,9)
val p7: Array[Int] = makePair(4, 9)
p7.foreach(println)


// multiple bounds
//T >: Lower <: Upper
//T <: Comparable[T] with Serializable with Cloneable
//T : Ordering : ClassTag


// type constraints
//T =:= U
// T <:< U
// T => U

//These constraints test whether T equals U, is a subtype of U, or is convertible
// to U. To use such a constraint, you add an “implicit evidence parameter” like
// this:
class Pair8[T](val first: T, val second: T)(implicit ev: T <:< Comparable[T])


def firstLast[A, C <: Iterable[A]](it: C) = (it.head, it.last)

// code error: Error:(292, 35) inferred type arguments [Nothing,List[Int]] do not
// conform to method firstLast's type parameter bounds [A,C <: Iterable[A]]
//val (a1: Int , b1: Int) = firstLast(List(1,2,3))

// The type inferencer cannot figure out A is from looking at List(1, 2, 3),
// because it matches A and C in a single step.

//val (a1: Int , b1: Int) = firstLast(List(1,2,3))
//println(a1 + " " + b1)

// this one works
def firstLast2[A, C](it: C)(implicit env: C <:< Iterable[A]) = (it.head, it.last)
val (a1: Int, b1: Int) = firstLast2(List(1, 2, 3))
println(a1 + " " + b1)


// this one also works, a lot more concise
def firstLast3[A](it: Iterable[A]) = (it.head, it.last)
val (a2: Int, b2: Int) = firstLast3(List(1, 2, 3))
println(a2 + " " + b2)

// variance
// The + means that the type is covariant in T—that is, it varies in the same direction.
//Since Student is a subtype of Person, a Pair[Student] is now a subtype of Pair[Person]
// - means in different direction, also called "contravariant"

trait Friend1[-T] {
  def befriend(someone: T)
}

class Person extends Friend1[Person] {
  override def befriend(someone: Person): Unit = println("good friends")
}

// student is a subtype of person
class Student extends Person

// because of the variance '-', Friend1[Student] and Friend1[Person] has the diffrent
// direction between Student and Person, so Friend1[Student] is a supertype of Friend1[Person]
def makeFriendWith(s: Student, f: Friend1[Student]): Unit = {
  f.befriend(s)
}


val susan = new Student
val fred = new Student

makeFriendWith(susan, fred)

val tom = new Student
val jerry = new Person

makeFriendWith(tom, jerry)


def getCol(n: Int, a: Array[Array[Int]]) = a.map {
  _ (n)
}
val arr = Array(Array(0, 1, 2), Array(1, 2, 3))
getCol(1, arr).foreach(print)


val aaa = "c" * 3
println(aaa)

