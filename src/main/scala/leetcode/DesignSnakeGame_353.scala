package leetcode

object DesignSnakeGame_353 extends App {

  class SnakeGame(_width: Int, _height: Int, _food: Array[Array[Int]]) {

    /* Initialize your data structure here.
    @param width - screen width
    @param height - screen height
    @param food - A list of food positions
        E.g food = [[1,1], [1,0]] means the first food is positioned at [1,1], the second is at [1,0]. */

    /* Moves the snake.
        @param direction - 'U' = Up, 'L' = Left, 'R' = Right, 'D' = Down
    @return The game's score after the move. Return -1 if game over.
        Game over when snake crosses the screen boundary or bites its body. */
    def move(direction: String): Int = {
      0
    }

  }

  /**
  * Your SnakeGame object will be instantiated and called as such:
  * var obj = new SnakeGame(width, height, food)
  * var param_1 = obj.move(direction)
  */
}
