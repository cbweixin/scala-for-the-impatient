package leetcode

object RotateImage extends App {
  val arr = Array(Array(5, 1, 9, 11), Array(2, 4, 8, 10), Array(13, 3, 6, 7), Array(15, 14, 12, 16))
  rotate(arr)
  arr.foreach(a => a.foreach(print))

  def rotate(matrix: Array[Array[Int]]): Unit = {
    val m: Int = matrix.length
    val n: Int = matrix(0).length
    for (i <- 0 until m) {
      for (j <- i + 1 until n) {
        val t = matrix(i)(j)
        matrix(i)(j) = matrix(j)(i)
        matrix(j)(i) = t
      }
    }

    matrix.foreach(a => {
      var i = 0
      var j = a.length - 1
      while (i < j) {
        val t = a(i)
        a(i) = a(j)
        a(j) = t
        i += 1
        j -= 1

      }
    })

  }
}
