package leetcode

object FindFirstAndLastPositionOfElementInSortedArray_34 extends App {

  searchRange(Array(5, 7, 7, 8, 8, 10), 8).foreach(println)
  searchRange(Array(5, 7, 7, 8, 8, 10), 6).foreach(println)
  Solution.searchRange(Array(5, 7, 7, 8, 8, 10), 6).foreach(println)
  Solution.searchRange(Array(5, 7, 7, 8, 8, 10), 8).foreach(println)

  def searchRange(nums: Array[Int], target: Int): Array[Int] = {
    Array(searchLeft(nums, target), searchRight(nums, target))
  }

  def searchLeft(nums: Array[Int], target: Int): Int = {
    var left = 0
    var right = nums.length - 1
    while (left < right) {
      val mid = left + (right - left) / 2
      if (target > nums(mid)) {
        left = mid + 1
      } else {
        right = mid
      }
    }
    if (left != nums.length && nums(left) == target) left else -1
  }

  def searchRight(nums: Array[Int], target: Int): Int = {
    var left = 0
    var right = nums.length - 1
    while (left < right) {
      val mid = right - (right - left) / 2
      if (target >= nums(mid)) {
        left = mid
      } else {
        right = mid - 1
      }
    }

    if (left != nums.length && nums(left) == target) left else -1
  }


  // TODO need take a look
  object Solution {
    def searchRange(nums: Array[Int], target: Int): Array[Int] = {

      val size = nums.size

      def findIndex(start: Int, end: Int): Int = {

        val i = (end + start) / 2
        nums(i) match {
          case `target` => i
          case _ if start == end => -1
          case _ if end - start == 1 =>
            if (nums(end) == target) end
            else -1
          case lower if lower > target => findIndex(start, i)
          case _ => findIndex(i, end)
        }
      }

      def range(bound: Int, dir: Int, i: Int): Int = {
        if (i == bound) i
        else if (nums(i + dir) == target)
          range(bound, dir, i + dir)
        else i


      }

      def findRange(i: Int): (Int, Int) = {
        if (i == -1) (-1, -1) else
          (range(0, -1, i), range(size - 1, 1, i))

      }

      if (size == 0) Array(-1, -1) else {
        val (a, b) = findRange(findIndex(0, size - 1))
        Array(a, b)
      }

    }

  }

}
