package leetcode

object MinimumMovesToEqualArrayElements_462 extends App {

  println(minMoves2(Array(1, 2, 3)))

  def minMoves2(nums: Array[Int]): Int = {
    val (numsSort, len) = (nums.sorted, nums.length)
    val median = numsSort(len / 2)
    numsSort.map(x => (x - median).abs).sum
  }

  def minMoves3(nums: Array[Int]): Int = {
    scala.util.Sorting.quickSort(nums)
    val median = nums(nums.length / 2)
    nums.map(x => (x - median).abs).sum
  }
}
