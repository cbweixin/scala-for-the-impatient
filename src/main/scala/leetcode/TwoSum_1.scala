package leetcode

import scala.collection.mutable

object TwoSum_1 extends App {
  val ar = twoSum(Array(3, 2, 4), 6)
  ar.foreach(println)
  def twoSum(nums: Array[Int], target: Int): Array[Int] = {
    val map = mutable.Map[Int, Int]()
    val arr = new Array[Int](2)
    var comp = 0

    for (i <- 0 until nums.length) {
      comp = target - nums(i)
      if (map.contains(comp) && i != map(comp)) {
        arr(0) = map(comp)
        arr(1) = i
        arr
      }
//      map += (nums(i)->i)
      map(nums(i)) = i
    }
    arr
  }

}
