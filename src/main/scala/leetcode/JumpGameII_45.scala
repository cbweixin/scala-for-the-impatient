package leetcode

object JumpGameII_45 extends App {

  def jump(nums: Array[Int]): Int = {
    var pre = 0
    var cur = 0
    var steps = 0

    for (i <- 0 until nums.length) {
      if (i > pre) {
        steps += 1
        pre = cur
      }
      cur = math.max(cur, i + nums(i))
    }

    steps
  }
}
