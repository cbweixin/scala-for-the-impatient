package leetcode

import scala.util.Sorting

object ArrayPartitionI_561 extends App {

  println(arrayPairSum(Array(1, 4, 3, 2)))

  def arrayPairSum1(nums: Array[Int]): Int = {
    def groupTwo(al: List[Int]): List[List[Int]] = al match {
      case Nil            => Nil
      case a :: b :: rest => List(a, b) :: groupTwo(rest)
    }

    Sorting.quickSort(nums)
    // reduceLeft vs foldLeft
    // reduceLeft don't need initial value, but foldLeft need
    groupTwo(nums.toList) map (x => x.head) reduceLeft ((x, y) => x + y)
  }

  def arrayPairSum(nums: Array[Int]): Int = {
    var s = 0
    Sorting.quickSort(nums)
//    for(i <- 0 to nums.length if i % 2 == 0){
    // another syntax of for loop
    for (i <- 0 to nums.length by 2) {
      s += nums(i)
    }
    s
  }

  def arrayPairSum2(nums: Array[Int]): Int = {
    import scala.util.Sorting
    Sorting.quickSort(nums)
    val ls = nums.toList
    ls.indices.filter(_ % 2 == 0).map(ls(_)).sum

  }

  def arrayPairSum3(nums: Array[Int]): Int = {
    Sorting.quickSort(nums)
    val ls = nums.toList
    ls.zipWithIndex.filter(_._2 % 2 == 0).map(_._1).sum

  }

  def arrayPairSum4(nums: Array[Int]): Int = {
    Sorting.quickSort(nums)
    val ls = nums.toList
    ls.zipWithIndex.collect {
      case (i, idx) if idx % 2 == 0 => i
    }.sum

  }
}
