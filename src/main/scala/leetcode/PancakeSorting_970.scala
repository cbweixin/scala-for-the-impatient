package leetcode

object PancakeSorting_970 extends App {
  println(pancakeSort(Array(3, 2, 4, 1)))

  def pancakeSort(A: Array[Int]): List[Int] = {

    var res = List[Int]()
    val N = A.length - 1
    var B = A
    for (i <- N to 0 by -1) {
      if (i + 1 != B(i)) {
        val k = B.indexOf(i + 1) + 1
        res = res ++: List(k, i + 1)
        B = B.slice(0, k).reverse ++: B.slice(k, N + 1)
        B = B.slice(0, i + 1).reverse ++: B.slice(i + 1, N + 1)
      }
    }

    res

  }

}
