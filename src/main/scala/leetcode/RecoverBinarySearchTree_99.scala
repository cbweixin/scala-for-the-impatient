package leetcode

object RecoverBinarySearchTree_99 extends App {

  //   Definition for a binary tree node.
  class TreeNode(var _value: Int) {
    var value: Int = _value
    var left: TreeNode = null
    var right: TreeNode = null
  }

  def recoverTree(root: TreeNode): Unit = {
    var first: TreeNode = null
    var second: TreeNode = null
    var pre: TreeNode = null

    def inorder(node: TreeNode): Unit = {
      if (node == null) {
        return
      }
      inorder(node.left)
      if (pre == null) {
        pre = node
      }
      if (pre.value > node.value) {
        if (first == null) {
          first = pre
        }
        second = node
      }
      pre = root
      inorder(node.right)
    }

    inorder(root)
    if (first != null && second != null) {
      val temp = first.value
      first.value = second.value
      second.value = temp
    }

    return root

  }

}
