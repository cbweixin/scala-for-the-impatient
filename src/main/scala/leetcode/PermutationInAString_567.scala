package leetcode

import scala.collection.mutable

object PermutationInAString_567 extends App {

  def checkInclusion(s1: String, s2: String): Boolean = {
    if (s1.length > s2.length) {
      return false
    }
    val mMap1 = new mutable.HashMap[Char, Int]().withDefaultValue(0)
    val mMap2 = new mutable.HashMap[Char, Int]().withDefaultValue(0)

    for (c <- s1) {
      mMap1.update(c, mMap1(c) + 1)
    }

    for (c <- 0 until s1.length) {
      mMap2.update(s2(c), mMap2(s2(c)) + 1)
    }

    if (mMap1 == mMap2) {
      return true
    }

    for (c <- s1.length until s2.length) {
      mMap2.update(s2(c), mMap2(s2(c)) + 1)
      mMap2.update(s2(c - s1.length), mMap2(s2(c - s1.length)) - 1)
      if (mMap2(s2(c - s1.length)) == 0) {
        // remove the key
        mMap2 -= s2(c - s1.length)
      }
      if (mMap1 == mMap2) {
        return true
      }
    }
    false

  }

  object Solution {

    def checkInclusion(s1: String, s2: String): Boolean = {
      if (s1.length > s2.length) return false
      val s1map = Array.ofDim[Int](26)
      val s2map = Array.ofDim[Int](26)
      for (i <- 0 until s1.length) {
        s1map(mapIdx(s1, i)) += 1
        s2map(mapIdx(s2, i)) += 1
      }
      for (i <- 0 until (s2.length - s1.length)) {
        if (checkMatch(s1map, s2map)) return true
        s2map(mapIdx(s2, i + s1.length)) += 1
        s2map(mapIdx(s2, i)) -= 1
      }
      checkMatch(s1map, s2map)
    }

    def mapIdx(s: String, i: Int) = s(i).toByte - 'a'.toByte

    def checkMatch(map1: Array[Int], map2: Array[Int]): Boolean = {
      map1.zip(map2).forall { case (c1, c2) => c1 == c2 }
    }
  }
}
