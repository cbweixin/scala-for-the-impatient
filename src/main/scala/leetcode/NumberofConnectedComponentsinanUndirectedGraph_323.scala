package leetcode

object NumberofConnectedComponentsinanUndirectedGraph_323 extends App {

  println(countComponents(5, Array(Array(0, 1), Array(1, 2), Array(3, 4))))

  class UnionFind(val nodes: Int) {
    // notice how to chnange a Range to Array
    private val group: Array[Int] = 0 until nodes toArray
    // notice how to quickly intialize and [1] * n array
    private val sizes: Array[Int] = Array.fill(nodes)(1)
    // scala automatically generate getter and setter for it
    var count = nodes

    def find(x: Int): Int = {
      var a = x
      while (a != group(a)) {
        group(a) = group(group(a))
        a = group(a)
      }
      a
    }

    def union(x: Int, y: Int) = {
      val xGroup = find(x)
      val yGroup = find(y)
      if (xGroup != yGroup) {
        if (sizes(xGroup) > sizes(yGroup)) {
          group(yGroup) = xGroup
          sizes(xGroup) += sizes(yGroup)
        } else {
          group(xGroup) = yGroup
          sizes(yGroup) += sizes(xGroup)
        }
        count -= 1
      }
    }

    // wihtout using sizes group
    def union2(x: Int, y: Int) = {
      val xGroup = find(x)
      val yGroup = find(y)
      if (xGroup != yGroup) {
        group(math.min(xGroup, yGroup)) = math.max(xGroup, yGroup)
        count -= 1
      }
    }
  }

  def countComponents(n: Int, edges: Array[Array[Int]]): Int = {
    val uf = new UnionFind(n)
    edges.foreach(a => {
      //      uf.union(a(0), a(1))
      uf.union2(a(0), a(1))
    })
    uf.count
  }

  object Solution {

    def countComponents(n: Int, edges: Array[Array[Int]]): Int = {
      val pArr = new Array[Int](n)
      pArr.indices.foreach(i => pArr(i) = i)

      def find(x: Int): Int = {
        if (pArr(x) == x) return x
        find(pArr(x))
      }

      edges.foreach(edge => {
        val x = find(edge(0))
        val y = find(edge(1))
        pArr(x) = y
      })
      pArr.indices.foreach(i => if (pArr(i) != i) pArr(i) = find(pArr(i)))
      pArr.distinct.length
    }
  }
}
