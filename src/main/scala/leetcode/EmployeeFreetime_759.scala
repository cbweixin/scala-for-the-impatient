package leetcode

import scala.collection.mutable.ListBuffer
import scala.util.Sorting

object EmployeeFreetime_759 extends App {

  employeeFreeTime(Array(Array(Array(1, 2), Array(5, 6)), Array(Array(1, 3)), Array(Array(4, 10))))
    .flatMap(x => x)
    .foreach(println)

  def employeeFreeTime(schedule: Array[Array[Array[Int]]]): Array[Array[Int]] = {

    var timeSlots = scala.collection.mutable.ListBuffer[(Int, Int)]()
    for (employees <- schedule) {
      for (times <- employees) {
        // how to append a tuple to ListBuffer?
        // https://stackoverflow.com/questions/26606986/scala-add-a-tuple-to-listbuffer
        timeSlots += ((times(0), times(1)))
      }
    }

    // knowledge , how scala sort tuple
    timeSlots = timeSlots.sortBy(x => (x._1, x._2))
    val res = scala.collection.mutable.ListBuffer[(Int, Int)]()
    var latest = timeSlots(0)._2
    for ((start, end) <- timeSlots) {
      if (start > latest) {
        res += ((latest, start))
      }
      latest = math.max(latest, end)
    }

    res.map(x => Array(x._1, x._2)).toArray
  }
}
