package leetcode

object RemoveDuplicateFromSortedArray_26 extends App {
  //  println(removeDuplicates(Array(0, 0, 1, 1, 1, 2, 2, 3, 3, 4)))
  println(removeDuplicates(Array()))

  def removeDuplicates_2(nums: Array[Int]): Int = {
    nums.distinct.length
  }

  def removeDuplicates(nums: Array[Int]): Int = {
    if (nums.isEmpty) {
      0
    } else {
      var start = 0
      var i = 1
      while (i < nums.length) {
        if (nums(i) != nums(start)) {
          start += 1
          nums(start) = nums(i)
        }
        i += 1
      }

      start + 1

    }

  }

  def removeDuplicates3(nums: Array[Int]): Int = {
    var start = 0
    var i = 1
    while (i < nums.length) {
      if (nums(i) != nums(start)) {
        start += 1
        nums(start) = nums(i)
      }
      i += 1
    }

    // headOption.isEmty is faster then nums.isEmpty
    if (nums.headOption.isEmpty) {
      0
    } else {
      start + 1
    }
  }

}
