package leetcode

import scala.collection.mutable.ListBuffer

object WordSquares_425 extends App {
  //  val res = wordSquares(Array("area", "lead", "wall", "lady", "ball"))
  val res = wordSquares(Array("abaa", "aaab", "baaa", "aaba"))
  //  res.flatMap(x => x).foreach(println)
  println(res.length)
  res.foreach(x => {
    println("=====")
    x.foreach(println)
  })

  class TrieNode() {
    val indices = ListBuffer[Int]()
    val children = Array.ofDim[TrieNode](26)

    def insert(word: String, i: Int) = {
      var cur = this
      for (c <- word) {
        cur.children(c - 'a') match {
          case node: TrieNode => cur = node
          case _ => {
            cur.children(c - 'a') = new TrieNode()
            cur = cur.children(c - 'a')
          }
        }
        cur.indices += i
      }
    }
  }

  def wordSquares(words: Array[String]): List[List[String]] = {
    val root = new TrieNode()
    val res = ListBuffer[List[String]]()

    def backtrack(temp: List[String], res: ListBuffer[List[String]]): Unit = {
      if (temp.length == words(0).length) {
        res += temp
        return
      }
      var node = root
      for (s <- temp) {
        node = node.children(s(temp.length) - 'a')
        if (node == null) {
          return
        }
      }

      for (idx <- node.indices) {
        val li = temp :+ words(idx)
        backtrack(li, res)
      }
    }

    for (i <- 0 until words.length) {
      root.insert(words(i), i)
    }

    for (w <- words) {
      val temp = List() :+ w
      backtrack(temp, res)
    }

    res.toList

  }
}
