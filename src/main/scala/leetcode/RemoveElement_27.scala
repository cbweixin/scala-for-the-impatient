package leetcode

object RemoveElement_27 extends App {
  println(removeElement(Array(3, 2, 2, 3), 3))
  println(removeElement(Array(3, 2, 2, 3), 2))
  println(removeElement(Array(0, 1, 2, 2, 3, 0, 4, 2), 2))

  def removeElement(nums: Array[Int], `val`: Int): Int = {
    if (nums == null || nums.length == 0) {
      return 0
    }
    var left = 0
    var right = nums.length - 1
    while (left < right) {
      if (nums(left) == `val`) {
        val t = nums(left)
        nums(left) = nums(right)
        nums(right) = t
        right -= 1
      } else {
        left += 1
      }
    }

    if (nums(left) == `val`) left else left + 1

  }

  object Solution {

    def removeElement(nums: Array[Int], `val`: Int): Int = {
      var current = 0
      for (i <- 0 to (nums.size - 1)) {
        if (nums(i) != `val`) {
          nums.update(current, nums(i))
          current += 1
        }
      }
      current
    }
  }
}
