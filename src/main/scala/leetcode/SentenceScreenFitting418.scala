package leetcode

object SentenceScreenFitting418 extends App {

  println(wordsTyping(Array("hello", "world"), 2, 8))
  println(wordsTyping(Array("a", "bcd", "e"), 3, 6))
  println(wordsTyping(Array("f", "p", "a"), 8, 7))

  def wordsTyping(sentence: Array[String], rows: Int, cols: Int): Int = {
    val s = sentence.mkString(" ", " ", "")
    var res = 0

    var i = 1
    // or for(j <- 0 to rows -1)
    for (j <- 0 until rows) {
      i += cols
      if (i > s.length - 1) {
        res += i / s.length
        i %= s.length
      }

      while (s.charAt(i) != ' ') {
        i -= 1
      }

      i += 1
    }

    res

  }
}
