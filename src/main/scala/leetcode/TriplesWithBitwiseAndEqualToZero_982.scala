package leetcode

object TriplesWithBitwiseAndEqualToZero_982 extends App {

  def countTriplets(A: Array[Int]): Int = {
    import scala.collection.mutable
    var mmap = new mutable.HashMap[Int, Int]().withDefaultValue(0)
    var res = 0

    for (i <- A; j <- A) {
      mmap.update(i & j, mmap(i & j) + 1)
    }

    for (k <- A; (l, v) <- mmap) {
      //    for (k <- A; l <- mmap.keys) {
      if ((k & l) == 0) {
        res += v
      }
    }

    res

  }
}
