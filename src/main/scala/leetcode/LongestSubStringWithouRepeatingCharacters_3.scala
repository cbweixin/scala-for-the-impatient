package leetcode

object LongestSubStringWithouRepeatingCharacters_3 extends App {
  println(Solution.lengthOfLongestSubstring("abcabcbb"))

  def lengthOfLongestSubstring(s: String): Int = {
    val pos = new scala.collection.mutable.HashMap[Char, Int]()
    val arr = s.toCharArray
    var start = 0
    var res = 0
    for (i <- 0 until arr.length) {
      if (!pos.contains(arr(i)) || pos.get(arr(i)).getOrElse(0) < start) {
        res = math.max(res, i - start + 1)
      } else {
        start = pos.get(arr(i)).getOrElse(0) + 1
      }
      pos.put(arr(i), i)
    }
    res
  }

  object Solution {

    def lengthOfLongestSubstring(s: String): Int = {
      // /: fold left operator https://www.scala-lang.org/old/node/1562
      // :\ fold right operator

      (("", 0) /: s) {
        // for test case "abcabcbb"
        // ("", 0) is the initial value, c is the character of s
        // c = a, newS = a
        // c = b, newS = b + a = ba
        // c = c, newS = c + b + a  = cba
        // c = a, newS = a + c + b, because a == a ( the last char in newS), so on
        case ((s, len), c) =>
          val newS = c + s.takeWhile(_ != c)
          println(newS)
          //          (newS, len.max(newS.size))
          //          (newS, math.max(len, newS.size))
          // always get the max value between len and newS.size
          (newS, len max newS.size)
      }._2
    }
  }
}
