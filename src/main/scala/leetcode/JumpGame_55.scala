package leetcode

object JumpGame_55 extends App {

  println(canJump(Array(2, 3, 1, 1, 4)))

  def canJump(nums: Array[Int]): Boolean = {
    var maxReach = nums(0)
    for (i <- 0 until nums.length) {
      if (i > maxReach) {
        return false
      }
      maxReach = math.max(maxReach, i + nums(i))
    }

    //    return maxReach >= nums.length - 1, no need to check any more, since for loop is complete already
    true

  }
}
