package leetcode

object LoggerRateLimiter_359 {

  class Logger() {

    /** Initialize your data structure here. */
    import scala.collection.mutable

    val messages = new mutable.HashMap[String, Int]()

    /** Returns true if the message should be printed in the given timestamp, otherwise returns false.
      * If this method returns false, the message will not be printed.
      * The timestamp is in seconds granularity. */
    def shouldPrintMessage(timestamp: Int, message: String): Boolean = {
      if (timestamp < messages.getOrElse(message, 0)) {
        return false
      }
      messages(message) = timestamp + 10
      true
    }

  }

  class Logger2() {

    /** Initialize your data structure here. */
    import scala.collection.mutable

    val map = new mutable.HashMap[String, Int]()

    /** Returns true if the message should be printed in the given timestamp, otherwise returns false.
      * If this method returns false, the message will not be printed.
      * The timestamp is in seconds granularity. */
    def shouldPrintMessage(timestamp: Int, message: String): Boolean = {
      map.get(message) match {
        case Some(lmts) =>
          if (timestamp >= lmts) {
            map(message) = timestamp + 10
            true
          } else false
        case None => {
          map(message) = timestamp + 10
          true
        }
      }
    }

  }

}
