package leetcode

object Sqrt_69 {

  def mySqrt(x: Int): Int = {
    if (x < 2) {
      return x
    }
    var left = 1
    var right = x / 2

    while (left <= right) {
      var mid = left + (right - left) / 2
      if (mid > x / mid) {
        right = mid - 1
      } else {
        left = mid + 1
      }
    }
    left - 1

  }

  import scala.annotation.tailrec

  object Solution {

    def mySqrt(x: Int): Int = {

      // notice the usage of tail recursion
      @tailrec
      def mySqrt(x: Int, div: Int): Int = {
        if (x / div < div) div - 1 else mySqrt(x, div + 1)
      }

      mySqrt(x, 1)

    }
  }
}
