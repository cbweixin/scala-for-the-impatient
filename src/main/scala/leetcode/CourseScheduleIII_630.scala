package leetcode

object CourseScheduleIII_630 extends App {

  def scheduleCourse(courses: Array[Array[Int]]): Int = {
    // bugfixed , notice we need to sort by ending time not duration
    var times: List[(Int, Int)] = courses.map(course => (course(0), course(1))).toList.sortBy(_._2)

    // by default , pq is max heap in scala
    val pq = scala.collection.mutable.PriorityQueue[Int]()
    var curTimes = 0
    // times.foreach(t=>{}) is right, times.foreach((k,v) => {}) is wrong
    times.foreach {
      case (duration, end) => {
        curTimes += duration
        pq.enqueue(duration)
        if (curTimes > end && !pq.isEmpty) {
          val t = pq.dequeue()
          curTimes -= t
        }
      }
    }
    pq.length
  }
}
