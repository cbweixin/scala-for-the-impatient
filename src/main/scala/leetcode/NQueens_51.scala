package leetcode

import scala.collection.mutable.ListBuffer

object NQueens_51 extends App {

  def solveNQueens(n: Int): List[List[String]] = {
    val res: ListBuffer[List[String]] = ListBuffer()

    def isValid(solution: List[Int], row: Int, col: Int): Boolean = {
      for (i <- 0 until row) {
        if (solution(i) == col || math.abs(row - i) == math.abs(col - solution(i))) {
          return false
        }
      }
      return true
    }

    def backtrack(solution: List[Int], row: Int): Unit = {
      if (row == n) {
        val t: List[String] = solution.map(x => "." * x + "Q" + "." * (n - x - 1))
        res.append(t)
      }

      for (i <- 0 until n) {
        if (isValid(solution, row, i)) {
          backtrack(solution :+ i, row + 1)
        }
      }

    }

    backtrack(List[Int](), 0)

    return res.toList
  }

  object Solution {

    def solveNQueens(n: Int): List[List[String]] = {

      bt(n, 0, Nil).map { validPos =>
        val arr = Array.fill(n)(Array.fill[Char](n)('.'))
        validPos.foreach {
          case (i, j) =>
            arr(i)(j) = 'Q'
        }
        arr.map(_.mkString).toList
      }.toList
    }

    def bt(n: Int, x: Int, queens: List[(Int, Int)]): Seq[List[(Int, Int)]] = {
      if (x == n) {
        Seq(queens)
      } else {
        val buf = scala.collection.mutable.ArrayBuffer[List[(Int, Int)]]()
        for (j <- 0 until n) {
          if (isSafe(x, j, queens)) {
            buf ++= bt(n, x + 1, (x, j) :: queens)
          }
        }
        buf
      }
    }

    def isSafe(x: Int, y: Int, queens: List[(Int, Int)]) = {
      queens.forall {
        case (qx, qy) =>
          x != qx && y != qy && x - y != qx - qy && x + y != qx + qy
      }
    }
  }
}
