package leetcode

import scala.collection.mutable

object CanIWin_464 extends App {

  println(canIWin(10, 11))
  println(canIWin(10, 0))
  println(canIWin(4, 6))

  def canIWin(maxChoosableInteger: Int, desiredTotal: Int): Boolean = {
    if (maxChoosableInteger >= desiredTotal) {
      return true
    }
    if ((1 + maxChoosableInteger) * maxChoosableInteger / 2 < desiredTotal) {
      return false
    }

    return canWin(maxChoosableInteger, desiredTotal, 0, mutable.HashMap.empty[Int, Boolean])

  }

  def canWin(maxNum: Int, total: Int, used: Int, dict: mutable.HashMap[Int, Boolean]): Boolean = {
    if (dict.contains(used)) {
      return dict(used)
    }

    for (i <- 0 until maxNum) {
      if (((1 << i) & used) == 0) {
        if (total <= i + 1 || !canWin(maxNum, total - i - 1, used | (1 << i), dict)) {
          dict(used) = true
          return true
        }
      }
    }
    dict(used) = false
    return false

  }

  object Solution {

    def canIWin(maxChoosableInteger: Int, desiredTotal: Int): Boolean = {

      // easy sum
      if (maxChoosableInteger * (maxChoosableInteger + 1) / 2 < desiredTotal) return false

      if (desiredTotal <= 0) return true

      val set = (1 to maxChoosableInteger).toSet

      val mem = new collection.mutable.HashMap[Set[Int], Boolean]()

      def canIWinRecursive(subSet: Set[Int], remaining: Int): Boolean = {

        if (mem.contains(subSet)) return mem.get(subSet).get

        if (remaining <= 0) return false

        //does set remove a element consant time operations? Shouldn't be

        for (next <- subSet) {
          if (!canIWinRecursive(subSet - next, remaining - next)) {
            mem.put(subSet, true)
            return true
          }
        }

        mem.put(subSet, false)
        return false
      }

      canIWinRecursive(set, desiredTotal)

    }
  }
}
