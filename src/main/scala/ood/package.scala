import JigSaw.Orientation.Orientation
import JigSaw.Shape.Shape

import scala.beans.BeanProperty
import scala.collection.immutable

package object JigSaw {

  object Orientation extends Enumeration {
    type Orientation = Value
    val LEFT = Value(1)
    val TOP = Value(2)
    val RIGHT = Value(3)
    val BOTTOM = Value(4)

    // Should stay in this order
    def getOpposite(orientation: Orientation): Orientation = orientation match {
      case LEFT =>
        RIGHT
      case RIGHT =>
        LEFT
      case TOP =>
        BOTTOM
      case BOTTOM =>
        TOP
      case _ =>
        null
    }
  }

  object Shape extends Enumeration {
    type Shape = Value
    val INNER = Value(1)
    val OUTER = Value(2)
    val FLAT = Value(3)

    def getOpposite(shape: Shape): Shape = shape match {
      case INNER =>
        OUTER
      case OUTER =>
        INNER
      case _ =>
        null
    }
  }

  class Edge(var shape: Shape, @BeanProperty var code: String // used to mock how pieces would fit together.
            ) {
    @BeanProperty
    var parentPiece: Piece = null


    def _createMatchingEdge: Edge = {
      if (shape == Shape.FLAT) return null
      new Edge(Shape.getOpposite(shape), getCode)
    }

    /* Check if this edge fits into the other one. */
    def fitsWith(edge: Edge): Boolean = edge.getCode == getCode


    /* Return the shape of the edge. */
    def getShape: Shape = shape

    override def toString: String = code
  }


  object Piece {
    private val NUMBER_OF_EDGES = 4
  }

  class Piece(val edgeList: Array[Edge]) {
    val orientationArray: Array[Orientation] = Array(Orientation.LEFT, Orientation.TOP, Orientation.RIGHT, Orientation.BOTTOM)
    var edges = new immutable.HashMap[Orientation, Edge]

    var i = 0

    while (i < edgeList.length) {
      val edge = edgeList(i)
      edge.setParentPiece(this)
      edges += (orientationArray(i) -> edge)
      i += 1
    }

    /* Set this edge in the appropriate orientation, rotating the piece as necessary. */
    def setEdgeAsOrientation(edge: Edge, orientation: Orientation): Unit = {
      val currentOrientation = getOrientation(edge)
      // original java code is like this:
      // rotateEdgesBy(orientation.ordinal() - currentOrientation.ordinal());
      // but for scala, there is no `ordinal`, so I hack it and give a seq ids to enummeration
      // then use `id` as `ordinal`
      rotateEdgesBy(orientation.id - currentOrientation.id)
    }

    /* Return the current orientation of the edge. */
    private def getOrientation(edge: Edge): Orientation = {
      val default = (Orientation.LEFT, edge)
      // TODO look at find usage for a map
      edges.find(_._2 == edge).getOrElse(default)._1
    }

    /* Rotate edges by "numberRotations". */
    def rotateEdgesBy(numberRotations: Int): Unit = {
      val orientations = Orientation.values
      var rotated = new immutable.HashMap[Orientation, Edge]
      var rotations = numberRotations % Piece.NUMBER_OF_EDGES
      if (rotations < 0) {
        rotations += Piece.NUMBER_OF_EDGES
      }

      /**
        *
        * for (int i = 0; i < orientations.length; i++) {
        * Orientation oldOrientation = orientations[(i - numberRotations + NUMBER_OF_EDGES) % NUMBER_OF_EDGES];
        * Orientation newOrientation = orientations[i];
        * 	rotated.put(newOrientation, edges.get(oldOrientation));
        * }
        */
      orientationArray.zipWithIndex.foreach {
        case (o: Orientation, i: Int) => {
          val oldIdx = (i - numberRotations + Piece.NUMBER_OF_EDGES) % Piece.NUMBER_OF_EDGES
          rotated += (o -> edges(orientationArray(oldIdx)))
        }
      }

      edges = rotated

    }

    /* Check if this piece is a corner piece.
    * public boolean isCorner() {
		    Orientation[] orientations = Orientation.values();
		    for (int i = 0; i < orientations.length; i++) {
			  Shape current = edges.get(orientations[i]).getShape();
			  Shape next = edges.get(orientations[(i + 1) % NUMBER_OF_EDGES]).getShape();
			  if (current == Shape.FLAT && next == Shape.FLAT) {
				return true;
			}
		}
		return false;
	}
   * */

    def isCorner: Boolean = {
      orientationArray.zipWithIndex.foreach {
        case (o: Orientation, i: Int) => {
          val current = edges(o).getShape
          val next = edges(orientationArray((i + 1) % Piece.NUMBER_OF_EDGES)).getShape
          if (current == Shape.FLAT && next == Shape.FLAT) {
            return true
          }
        }
      }

      false
    }

    /* Check if this piece has a border edge. */
    def isBorder: Boolean = {
      orientationArray.zipWithIndex.foreach {
        case (o: Orientation, i: Int) => {
          val current = edges(o).getShape
          if (current == Shape.FLAT) {
            return true
          }
        }
      }
      false
    }

    /* Get edge at this orientation. */
    def getEdgeWithOrientation(orientation: Orientation): Edge = edges(orientation)

    /* Return the edge that matches targetEdge. Returns null if there is no match. */
    def getMatchingEdge(targetEdge: Edge): Edge = {
      import scala.collection.JavaConversions._
      for (e <- edges.values) {
        if (targetEdge.fitsWith(e)) return e
      }
      null
    }

    override def toString: String = {
      val sb = new StringBuilder
      val orientations = Orientation.values
      for (o <- orientations) {
        sb.append(edges.get(o).toString + ",")
      }
      "[" + sb.toString + "]"
    }
  }

}
