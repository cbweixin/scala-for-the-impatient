package chapt7

import java.util

/**
  * ex 1
  * Write an example program to demonstrate that
  * package com.horstmann.impatient
  * is not the same as
  * package com
  * package horstmann
  * package impatient
  */
package com.horstman.impatient {

//  object fromImpatient {
//    val value = 4
//  }

  object fromImpatient2 {
    val value = 5
  }

}

package com {

  object fromCom {
    val value = 1
  }

  /*
   <code>private[com]</code> is like the `private class` in java, it makes <code>giveRaise</code> only
   visible inside com package and all its sub-packages
   */
  object VisibilityDef {
    private[com] def giveRaise(rate: Double) = rate * 0.5
  }

  object VisibilityUsage {
    println(VisibilityDef.giveRaise(2))
  }

  package horstman {

    object fromHostman {
      val value = 2
    }

    package impatient {

      object fromImpatient {
        val value = 3
      }

    }

  }

}

/**
  * ex 2
  * Write a puzzler that baffles your Scala friends, using a package com that isn’t at the top level.
  *
  */
package puzzler {

  package com {

    object fromCom {
      val value = 23
    }
  }
}

/**
  * ex 3
  *
  * Write a package random with functions nextInt(): Int, nextDouble(): Double,
  * and setSeed(seed: Int): Unit. To generate random numbers, use the linear congruential generator
  * next = (previous × a + b) mod 2n,
  * where a = 1664525, b = 1013904223, n = 32, and the initial value of previous
  * is seed.
  *
  */
package object random {
  private val b: Int = 1013904223
  private var seed: Int = 0
  private val a: Int = 1664525

  def nextInt(): Int = {
    seed = ((seed * a + b) % (1L << 32)).toInt
    if (seed < 0) {
      seed = ~seed
    }
    seed
  }

  def nextDouble(): Double = {
    ((seed * a + b)) % (1L << 32) * 1.0 / 10000L
  }

  def setSeed(seed: Int): Unit = this.seed = seed
}

/**
  * Task 4:
  *
  * <p>Why do you think the Scala language designers provided the package object syntax instead
  * of simply letting you add functions and variables to a package?
  *
  * <p>Solution: <br/>
  * They decided to make it explicit by adding just one word "object" to package declaration,
  * in my opinion, for a couple of reasons:
  * <ul>
  * <li>since its possible to have package declarations in different files, it would be hard
  * to maintain functions and variable in different places for the same package</li>
  * <li>because variables in package object are global (singletons) they didn't want to make it
  * available by default</li>
  * </ul>
  */
object Exercises extends App {

  println("ex 1 ============")
  val a = com.fromCom.value
  val b = com.horstman.fromHostman.value
  val c = com.horstman.impatient.fromImpatient.value
  val d = com.horstman.impatient.fromImpatient2.value

  println(a)
  println(b)
  println(c)
  println(d)

  println("ex 2 ============")
  println(puzzler.com.fromCom.value)

  println("ex 3 ============")
  println(random.nextInt())
  println(random.nextDouble())
  random.setSeed(1000)
  println(random.nextInt())
  println(random.nextDouble())

  println("ex 5 ============")
  com.VisibilityUsage
  // you won't be able to access from outside of `com` package
  //  println(com.VisibilityDef.giveRaise(2))

  println("ex 6 ============")

  import java.util.{HashMap => JavaHashMap}

  val jMap = new JavaHashMap[String, Int]
  jMap.put("a", 1)
  jMap.put("b", 2)
  jMap.put("c", 3)

  CopyEntryies.fromJavaHashMap(jMap).foreach {
    case (k, v) => {
      println(s"key is : ${k}, value is : ${v}")
    }
  }

  println("ex 9 ============")
  val up = UserNamePassword

  println("ex 10 ============")
  val overridesEx = JavaLangOverrides

  /**
    * ex 6, 7
    * Write a program that copies all elements from a Java hash map
    * into a Scala hash map. Use imports to rename both classes.
    */
  object CopyEntryies {

    import java.util.{HashMap => JavaHashMap}
    import scala.collection.mutable.{HashMap => ScalaHashMap}

    def fromJavaHashMap(javaHashMap: JavaHashMap[String, Int]): ScalaHashMap[String, Int] = {
      import scala.collection.JavaConversions.iterableAsScalaIterable
      val result = new ScalaHashMap[String, Int]
      for (entry <- javaHashMap.entrySet()) {
        result(entry.getKey) = entry.getValue
      }
      result
    }
  }

  /**
    * Write a program that imports the java.lang.System class, reads the user name
    * from the user.name system property, reads a password from the StdIn object,
    * and prints a message to the standard error stream if the password is not "secret".
    * Otherwise, print a greeting to the standard output stream.
    * Do not use any other imports, and do not use any qualified names (with dots).
    *
    */
  object UserNamePassword {
    import java.lang.System._

    val userName: String = getProperty("user.name")
    val password: String = readLine("please enter password: ")
    if (password != "secret") {
      err.println("wrong password")
    } else {
      println(s"welcome : ${userName}")
    }
  }

  object JavaLangOverrides {}

  val overrides = List[Class[_]](
    classOf[Boolean],
    classOf[Byte],
    classOf[Double],
    classOf[Float],
    classOf[Long],
    classOf[Short],
    classOf[StringBuilder],
    classOf[Iterable[_]])

  println(overrides.mkString("\n"))
}
