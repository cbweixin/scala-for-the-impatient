package chapt1

import math.BigInt.probablePrime
import util.Random

class Ex{

  /**
    * What do you need to import so that you can get a random prime as probablePrime(100, Random),
    * without any qualifiers before
    * probablePrime and Random?
    */

   def getPrime() = println(probablePrime(100,Random))

}


