package chapt1

import scala.math.BigInt.probablePrime
import scala.util.Random

object Exercise2 {
  /**
    * What do you need to import so that you can get a random prime as probablePrime(100, Random),
    * without any qualifiers before
    * probablePrime and Random?
    */
  private def getPrime() = println(probablePrime(100, Random))

  /**
    * ex 6
    * Using BigInt, compute 21024.
    *
    * @return
    */
  private def getPow(): BigInt = BigInt(2).pow(1024)

  /**
    * ex 8
    * One way to create random file or directory names is to produce a random BigInt and convert it to base 36,
    * yielding a string such as "qsnvbevtomcj38o06kul". Poke around Scaladoc to find a way of doing this in Scala.
    */
  private def getString() = println(BigInt(128, Random).toString(36))

  /**
    * ex 9 How do you get the first character of a string in Scala? The last character?
    *
    * @param str
    * @return
    */
  private def getFirstChar(str: String): Character = str.charAt(0)

  private def getFirstChar2(str: String): Character = str.head

  private def getLastChar(str: String): Character = str.last

  /**
    * What do the take, drop, takeRight, and dropRight string functions do? What advantage or disadvantage do they
    * have over using substring?
    *
    * @param str
    */
  private def arrUtilitis(str: String): Unit = {
    println(str.take(str.length - 3))
    println(str.takeRight(3))
    println(str.drop(2))
    println(str.dropRight(2))
  }

  def main(args: Array[String]): Unit = {
    getPrime()
    getString()
    println(getPow().toString())
    println(getFirstChar("hello, world"))
    println(getLastChar("hello, world !"))
    arrUtilitis("hello,world!")
  }

}
