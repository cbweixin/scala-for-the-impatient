package chapt18

object Exercises extends App {

  println("=================================")
  val p1 = new Pair("first", "second")
  val p2 = p1.swap
  println(p1)
  println(p2)

  println("=================================")
  val p3 = new MutablePair[String]("hello", "world")
  val p4 = p3.swap()
  println(p3)
  println(p4)

  /**
    * task 1 - Define an immutable class Pair[T, S] with a method swap that returns a new pair with the components swapped.
    *
    * @param first
    * @param second
    * @tparam T
    * @tparam S
    */
  class Pair[T, S](val first: T, val second: S) {
    def swap(): Pair[S, T] = new Pair(second, first)
  }

  class MutablePair[T](var first: T, var second: T) {

    def swap() = {
      var temp = first
      first = second
      second = temp
    }
  }

}
