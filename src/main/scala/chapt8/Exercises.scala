package chapt8

import chapt8.items.{Bundle, Item, SimpleItem}
import chapt8.point.LabeledPoint
import chapt8.toys._

object Exercises extends App {

  println("ex 1 =============")
  val acct = new BankAccount(100)
  acct.deposit(100)
  println(acct)
  acct.withdraw(150)
  println(acct)
  val myAccount = new MyAccount(100, 3)
  myAccount.deposit(200)
  println(myAccount)
  myAccount.withdraw(150)
  println(myAccount)

  println("ex 2 =============")
  val acct2 = new SavingAccount(100, 0.04, 3, 2)
  acct2.deposit(100)
  println(acct2)
  acct2.withdraw(100)
  println(acct2)
  acct2.deposit(100)
  println(acct2)
  acct2.withdraw(100)
  println(acct2)
  acct2.deposit(100)
  println(acct2)
  acct2.withdraw(100)
  println(acct2)
  acct2.deposit(100)
  println(acct2)
  acct2.withdraw(100)
  println(acct2)

  acct2.earnMonthlyInterest
  println(acct2)

  println("ex 3 =============")
  val l: List[Shape with Drawable] = List(new Rectange(Point(1, 1), Point(10, 20)), new Circle(Point(2, 2), 5))
  for (s <- l) {
    s.draw
  }

  println("ex 4 =============")

  var l1: List[Item] = List(
    new SimpleItem("iMac 21", 1500),
    new SimpleItem("iPhone 4s", 800)
  )

  val b = new Bundle
  b.add(new SimpleItem("iPad2", 500))
  b.add(new SimpleItem("MacBook Air 13", 1200))

  l1 = b :: l1
  l1.foreach(println)

  println("ex 5 =============")
  // name conflicts with Point in toys package
  val p1 = new point.Point(1, 1)
  val p2 = new LabeledPoint("same point", 1, 1)
  println(p1)
  println(p2)

  println("ex 6 =============")
  val rec = new shapewithcenter.Rectangle(1, 2, 3, 4)
  println(rec)

  val circle = new shapewithcenter.Circle(new point.Point(1, 2), 3)
  println(circle)

  /**
    * ex 1
    *
    * Extend the following BankAccount class to a CheckingAccount class that charges $1 for every deposit and withdrawal.
    * class BankAccount(initialBalance: Double) {
    * private var balance = initialBalance
    * def currentBalance = balance
    * def deposit(amount: Double) = { balance += amount; balance }
    * def withdraw(amount: Double) = { balance -= amount; balance }
    * }
    *
    * @param initialBalance
    */
  class BankAccount(initialBalance: Double) {
    private var balance = initialBalance

    def deposit(amount: Double) = {
      balance += amount
      balance
    }

    def withdraw(amount: Double) = {
      balance -= amount
      balance
    }

    def getBalance: Double = balance

    override def toString: String = "balance = %f".format(balance)
  }

  class MyAccount(initialBalance: Double, val commission: Double) extends BankAccount(initialBalance: Double) {
    override def deposit(amount: Double): Double = super.deposit(amount - commission)

    override def withdraw(amount: Double): Double = super.withdraw(amount + commission)
  }

  /**
    * ex 2
    * Extend the BankAccount class of the preceding exercise into a class SavingsAccount that earns interest
    * every month (when a method earnMonthlyInterest is called) and has three free deposits or withdrawals
    * every month. Reset the transaction count in the earnMonthlyInterest method.
    *
    * @param initialBalance
    * @param interestMonth
    * @param freeTransaction
    * @param commission
    */
  class SavingAccount(
    initialBalance: Double,
    val interestMonth: Double = 0.0,
    val freeTransaction: Int = 3,
    val commission: Double = 1.0)
      extends BankAccount(initialBalance) {
    var transactionInMonth: Int = 0

    def isFreeTransaction = transactionInMonth <= freeTransaction

    override def deposit(amount: Double): Double = {
      transactionInMonth += 1
      super.deposit(amount - (if (isFreeTransaction) 0 else commission))
    }

    override def withdraw(amount: Double) = {
      transactionInMonth += 1
      super.withdraw(amount + (if (isFreeTransaction) 0 else commission))
    }

    def earnMonthlyInterest = {
      transactionInMonth = 0
      super.deposit(getBalance * interestMonth)
    }
  }

}

/**
  * ex 3
  * Consult your favorite Java or C++ textbook which is sure to have an
  * example of a toy inheritance hierarchy, perhaps involving employees,
  * pets, graphical shapes, or the like. Implement the example in Scala.
  */
package toys {

  abstract class Shape {
    val name = "Abstract shape"

    override def toString = name
  }

  trait Drawable {
    def draw = println(toString)
  }

  class Point(val x: Int, val y: Int) {
    override def toString = "%d,%d".format(x, y)
  }

  // companion
  object Point {
    def apply(x: Int = 0, y: Int = 0) = new Point(x, y)
  }

  class Rectange(val topLeft: Point, val bottomRight: Point) extends Shape with Drawable {
    override val name = "Rectangel"

    override def toString = {
      "%s (%s - %s)".format(name, topLeft.toString, bottomRight.toString)
    }
  }

  class Circle(val center: Point, val radius: Int) extends Shape with Drawable {
    override val name = "Circle"

    override def toString = {
      "%s (%s: %d)".format(name, center.toString, radius)
    }
  }

}

/**
  * ex 4
  *
  * Define an abstract class Item with methods price and description. A SimpleItem
  * is an item whose price and description are specified in the constructor.
  * Take advantage of the fact that a val can override a def. A Bundle is an item
  * that contains other items. Its price is the sum of the prices in the bundle.
  * Also provide a mechanism for adding items to the bundle and a suitable description method.
  *
  */
package items {

  abstract class Item {
    def description: String

    def price: Double

    override def toString = "%s(%s: %f)".format(this.getClass.getSimpleName, description, price)
  }

  class SimpleItem(
    override val description: String,
    override val price: Double
  ) extends Item

  class Bundle extends Item {
    private var items: List[Item] = List()

    def add(item: Item) = {
      items = item :: items
    }

    def price: Double = items.map(_.price).sum

    def description: String = items.map(_.description).mkString(", ")
  }
}

/**
  * ex 5
  * Design a class Point whose x and y coordinate values can be provided in a constructor.
  * Provide a subclass LabeledPoint whose constructor takes a label value and x and y coordinates, such as
  * new LabeledPoint("Black Thursday", 1929, 230.07)
  */
package point {

  class Point(val x: Double, val y: Double) {
    override def toString = "Point(%d, %d)".format(x, y)
  }

  class LabeledPoint(val label: String, x: Int, y: Int) extends Point(x, y) {
    override def toString = {
      "LabeledPoint(%s, %d, %d)".format(label, x, y)
    }
  }

}

/** ex 6
  * Define an abstract class Shape with an abstract method centerPoint and
  * subclasses Rectangle and Circle. Provide appropriate constructors for the
  * subclasses and override the centerPoint method in each subclass.
  */
package shapewithcenter {

  import point.Point

  abstract class Shape {
    def centerPoint: Point
  }

  class Circle(override val centerPoint: Point, val radius: Double) extends Shape

  class Rectangle(val x1: Double, val y1: Double, val x2: Double, val y2: Double) extends Shape {
    override def centerPoint: Point = new Point((x1 + x2) / 2, (y1 + y2) / 2)
  }

}
